//
//  BaseTabBarController.swift
//  AppStoreJsonAPI
//
//  Created by 김윤석 on 2021/04/25.
//

import UIKit

class BaseTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [
            createNavController(viewController: UIViewController(), title: "Today"),
            createNavController(viewController: UIViewController(), title: "Apps"),
            createNavController(viewController: AppsSearchCollectionViewController(), title: "Search")
        ]
    }
    
    fileprivate func createNavController(viewController: UIViewController, title: String) -> UIViewController {
        
        viewController.navigationItem.title = title
        viewController.view.backgroundColor = .white
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.prefersLargeTitles = true
        navController.tabBarItem.title = title
        
        return navController
    }
}
