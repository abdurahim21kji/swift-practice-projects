//
//  SearchResult.swift
//  AppStoreJsonAPI
//
//  Created by 김윤석 on 2021/04/25.
//

import Foundation
import UIKit

struct SearchResult: Decodable {
    let resultCount: Int
    let results: [Result]
}

struct Result: Decodable {
    let trackName: String
    let primaryGenreName: String
    let averageUserRating: Float?
    
    let screenshotUrls: [String]
    let artworkUrl100: String // icon
}
