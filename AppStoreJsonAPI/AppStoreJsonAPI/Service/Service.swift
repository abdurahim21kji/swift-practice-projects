//
//  Service.swift
//  AppStoreJsonAPI
//
//  Created by 김윤석 on 2021/04/25.
//

import Foundation

class Service{
    static let shared = Service()
    
    func fetchApps(searchTerm: String, completion: @escaping ([Result], Error?)->()){
        let urlString = "http://itunes.apple.com/search?term=\(searchTerm)&entity=software"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil)
                return
            }
            
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(SearchResult.self, from: data)
                
                completion(searchResult.results, nil)
                
            } catch let Jsonerror {
                completion([], Jsonerror)
                print(Jsonerror.localizedDescription)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
