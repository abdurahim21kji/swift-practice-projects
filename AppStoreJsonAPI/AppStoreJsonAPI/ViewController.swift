//
//  ViewController.swift
//  AppStoreJsonAPI
//
//  Created by 김윤석 on 2021/04/25.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        view.backgroundColor = .blue
        
        viewControllers = [
            UIViewController(),
            UIViewController(),
            UIViewController()
        ]
    }


}

