//
//  CompanyCell.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/04.
//

import UIKit

class CompanyCell: UITableViewCell {
    
    var company: Company? {
        didSet {
            //companyProfileImage.image = UIImageView(image: UIImage(data: company?.imageData))
            
            if let imageData = company?.imageData {
                companyProfileImage.image = UIImage(data: imageData)
            } else{
                companyProfileImage.image = UIImage(named: "plus")
            }
            
            if let name = company?.name, let founded = company?.founded {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM dd, yyyy"
                let foundedString = dateFormatter.string(from: founded)
                
                nameFoundedLabel.text = "\(name) - Founded: \(foundedString)"
                
            } else {
                nameFoundedLabel.text = company?.name
            }
            
            nameFoundedLabel.textColor = .white
            nameFoundedLabel.font = UIFont.boldSystemFont(ofSize: 16)
            
            //nameFoundedLabel.text = "\(company?.name) - Founded: \(company?.founded)"
        }
    }
    
    let companyProfileImage: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "plus"))
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let nameFoundedLabel: UILabel = {
        let label = UILabel()
        label.text = "Company Name"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .tealColor
        
        addSubview(companyProfileImage)
        companyProfileImage.heightAnchor.constraint(equalToConstant: 40).isActive = true
        companyProfileImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
        companyProfileImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        companyProfileImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        addSubview(nameFoundedLabel)
        //        nameFoundedLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        //        nameFoundedLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameFoundedLabel.leftAnchor.constraint(equalTo: companyProfileImage.rightAnchor, constant: 8).isActive = true
        nameFoundedLabel.centerYAnchor.constraint(equalTo: companyProfileImage.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
