//
//  CoreDataManager.swift
//  CoreDataPractice
//
//  Created by 김윤석 on 2021/06/03.
//

import Foundation
import CoreData


struct CoreDataManager {
    static let shared = CoreDataManager()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreDataPractice")
        container.loadPersistentStores { storeDescription, error in
            if let error = error {
                fatalError("\(error)")
            }
        }
        return container
    }()
    
    func fetchCompanies()-> [Company]{
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<Company>(entityName: "Company")
        
        do {
            let companies = try context.fetch(fetchRequest)
//            self.companies = companies
//            tableView.reloadData()
            return companies
        } catch let error {
            print(error)
            return []
        }
        
    }
}

