//
//  LongViewController.swift
//  FsCalendarPractice
//
//  Created by 김윤석 on 2021/04/11.
//

import UIKit

class LongViewController: UIViewController {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(gestureFired(_:)))
//        //gestureRecognizer.direction = .up
//        gestureRecognizer.direction = .down
//        gestureRecognizer.numberOfTouchesRequired = 1
//        contentView.addGestureRecognizer(gestureRecognizer)
//        contentView.isUserInteractionEnabled = true
        
        scroll.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func gestureFired(_ gesture: UISwipeGestureRecognizer) {
        if let fireView = gesture.view{
            fireView.backgroundColor = .yellow
        }
    }

}
