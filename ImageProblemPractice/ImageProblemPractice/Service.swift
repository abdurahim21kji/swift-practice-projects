//
//  Service.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//
import UIKit


// psick univ id: UCGX5sP4ehBkihHwt5bs5wvg
//key : AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=20


class Service {
    static let shared = Service()
    
    func fetchApps(searchTerm: String, completion: @escaping ([Item], Error?)->()){
        let urlString = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=20"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil)
                return
            }
            
            //success
            //            print(data)
            //            print(String(data: data!, encoding: .utf8))
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(ServiceResult.self, from: data)
                //print(searchResult.results.forEach({ print($0.trackName, $0.primaryGenreName)}))
                print(searchResult.items.forEach({
                    print($0.id)
                    print($0.snippet.thumbnails.medium.url)
                }))
                //self.appResult = searchResult.results
                //                DispatchQueue.main.async {
                //                    self.collectionView.reloadData()
                //                }
                //completion(searchResult.results, nil)
                completion(searchResult.items, nil)
                
            } catch let Jsonerror {
                completion([], Jsonerror)
                print(Jsonerror.localizedDescription)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
