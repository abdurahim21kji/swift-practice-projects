//
//  ViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/21.
//

import UIKit

class MyRoutineViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustInputViewPositionAndCollectionViewSize), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustInputViewPositionAndCollectionViewSize), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        collectionView.dataSource = self
        
        TodoManager.shared.todos = Storage.retrive("todos.json", from: .documents, as: [Todo].self) ?? []
        
        for index in 0..<TodoManager.shared.todos.count {
            if TodoManager.shared.todos[index].isCompleted {
                TodoManager.shared.increaseNumberOfCompletedWorkout()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.collectionView.reloadData()
//
//        //style
//        self.inputTextField.translatesAutoresizingMaskIntoConstraints = false
//        self.inputTextField.backgroundColor = .systemTeal
//        self.inputTextField.placeholder = "Put your work out"
//        self.inputTextField.layer.cornerRadius = 60/4
//
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(_:)))
//        self.inputTextField.addGestureRecognizer(tap)
//
        //layout
        
        animateTable()

        
    }
    
    @objc func tapped(_ recognizer: UITapGestureRecognizer){
        if recognizer.state == UITapGestureRecognizer.State.ended{
            print("tapped")
            doAnimation()

        }
    }
    
    func doAnimation(){
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.1,
                                                       delay: 0,
                                                       options: []) {
            //style
            self.inputTextField.backgroundColor = .white
            self.inputTextField.textColor = .green
            self.inputTextField.layer.borderWidth = 1
            self.inputTextField.layer.borderColor = self.inputTextField.textColor?.cgColor
            
            //move
            let transpose = CGAffineTransform(translationX: -8, y: -24)
            let scale = CGAffineTransform(scaleX: 0.7, y: 0.7)
            self.inputTextField.transform = transpose.concatenating(scale)
            
        } completion: { (position) in
            
        }
    }
    
    func undoAnimation(){
        let size = UIViewPropertyAnimator(duration: 0.1, curve: .linear) {
            self.inputTextField.backgroundColor = .systemGray5
            self.inputTextField.textColor = .systemGray
            self.inputTextField.layer.borderWidth = 0
            self.inputTextField.layer.borderColor = UIColor.clear.cgColor
            
            self.inputView?.transform = .identity
        }
        size.startAnimation()
    }
    
    func animateTable(){
        collectionView.reloadData()
        let cells = collectionView.visibleCells
        let tableViewHeight = collectionView.bounds.size.height
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        for cell in cells {
            UIView.animate(withDuration: 1.75,
                           delay: Double(delayCounter) * 0.05,
                           usingSpringWithDamping: 0.8,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: {cell.transform = CGAffineTransform.identity },
                           completion: nil)
            delayCounter += 1
        }
    }
    
    @IBOutlet weak var inputVewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Interface for adding workout
    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBAction func addButtonDidTapped(_ sender: Any) {
        TodoManager.shared.createTodo(contents: inputTextField.text ?? "", count: 1)
        inputTextField.text = ""
        Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
        collectionView.reloadData()
    }
    
}
