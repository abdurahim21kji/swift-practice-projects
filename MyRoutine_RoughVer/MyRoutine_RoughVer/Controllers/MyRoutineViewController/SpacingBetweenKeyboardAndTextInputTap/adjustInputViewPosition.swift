//
//  adjustInputViewPosition.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/23.

import UIKit

extension MyRoutineViewController {
    @objc func adjustInputViewPositionAndCollectionViewSize(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        // 키보드 높이에 따른 인풋뷰 위치 변경
        guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification {
            let adjustmentHeight = keyboardFrame.height - view.safeAreaInsets.bottom
            
            print(inputViewBottom.constant)
            inputViewBottom.constant = adjustmentHeight
            print(inputViewBottom.constant)
            collectionViewBottom.constant = (adjustmentHeight + inputVewHeight.constant) * -1
        } else {
            inputViewBottom.constant = 0
            collectionViewBottom.constant = (inputVewHeight.constant) * -1
        }
    }
}
