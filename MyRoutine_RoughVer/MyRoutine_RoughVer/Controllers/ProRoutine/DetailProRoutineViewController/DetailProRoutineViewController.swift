//
//  DetailProRoutineViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/23.
//

import UIKit

class DetailProRoutineViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        //guard let targetMuscle = self.targetMuscle else { return }
        //todos = WorkOutList.init(typesOfWorkOut: targetMuscle).getWorkOuts()
        
        todos.append(Todo(contents: "Chest Fly", count: 4, isCompleted: false))
        todos.append(Todo(contents: "Machine Incline Press", count: 4, isCompleted: false))
        todos.append(Todo(contents: "Incline Chest Fly", count: 4, isCompleted: false))
        todos.append(Todo(contents: "Dumbell Incline Press", count: 4, isCompleted: false))
        todos.append(Todo(contents: "High Cable Fly", count: 4, isCompleted: false))
    }
    
    let listOfWorkout = ["Chest Fly", "Machine Incline Press", "Incline Chest Fly", "Dumbell Incline Press", "High Cable Fly"]
    
    @IBOutlet weak var tableview: UITableView!
    
    var proName: String?
    var targetMuscle: String?
    
    var todos = [Todo]()
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Workout List"
        }
        else if section == 1{
            return "Routine List"
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

