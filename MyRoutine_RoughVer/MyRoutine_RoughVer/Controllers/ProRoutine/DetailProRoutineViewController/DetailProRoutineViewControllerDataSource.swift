//
//  DetailProRoutineViewControllerDataSource.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit
extension DetailProRoutineViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let proName = self.proName,
              let cell = tableview.dequeueReusableCell(withIdentifier: "SpecificWorkOutListCell") as? SpecificWorkOutListCell else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            cell.workOut.text = listOfWorkout[indexPath.row]
            playButton(for: cell, targetMuscle: proName + todos[indexPath.row].contents, num: indexPath.row)
        }
        else if indexPath.section == 1 {
            cell.workOut.text = "\(todos[indexPath.row].contents) X \(todos[indexPath.row].count)"
            cell.playButton.isHidden = true
            return cell
        }
        return cell
    }
}

private extension DetailProRoutineViewController {
    func playButton(for cell: SpecificWorkOutListCell, targetMuscle: String, num: Int) {
        
        cell.playButtonTapHandler = {
            Requester().request(contents: targetMuscle) { (response) in
                DispatchQueue.main.async {
                    guard let targetMuscleVideoPlayerViewController = self.storyboard?.instantiateViewController(identifier: "TargetMuscleVideoPlayerViewController") as? TargetMuscleVideoPlayerViewController else {return}
                    guard let videoId = response.items.first?.id.videoId else { return }
                    targetMuscleVideoPlayerViewController.videoID = videoId
                    self.present(targetMuscleVideoPlayerViewController, animated: true, completion: nil)
                }
            }
        }
    }
}
