//
//  ListOfProViewControllerDelegate.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/27.
//
import UIKit

extension ListOfProViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let proRoutineViewController = storyboard?.instantiateViewController(identifier: "ProRoutineViewController") as? ProRoutineViewController else {return}
        proRoutineViewController.selectedProName = namesOfPro[indexPath.row]
        self.navigationController?.pushViewController(proRoutineViewController, animated: true)
    }
}
