//
//  ProRoutineViewControllerDataSource.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit


extension ProRoutineViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedProName == "조준"{
            return ChoJunWorkOutSectionedByBodyParts[section].count
        }
        else if selectedProName == "황철순"{
            return HwangChulSoonWorkOutSectionedByBodyParts[section].count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WorkOutBodyPartListCell") as? WorkOutBodyPartListCell else { return UITableViewCell() }

        if selectedProName == "조준" {
            cell.bodyPartLabel.text = ChoJunWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
        }
        else if selectedProName == "황철순" {
            cell.bodyPartLabel.text = HwangChulSoonWorkOutSectionedByBodyParts[indexPath.section][indexPath.row]
        }
        
        return cell
    }
}
