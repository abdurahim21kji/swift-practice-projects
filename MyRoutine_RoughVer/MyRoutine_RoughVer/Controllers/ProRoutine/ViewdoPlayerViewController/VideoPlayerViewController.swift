//
//  VideoPlayerViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/24.
//

import UIKit
import WebKit

class TargetMuscleVideoPlayerViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    loadYoutube(videoID: self.videoID)
        
    }
    
    var videoID: String = ""
    
    @IBOutlet weak var webViewPlayer: WKWebView!
}

private extension TargetMuscleVideoPlayerViewController {
    func loadYoutube(videoID:String) {
        guard
            let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        
        webViewPlayer.load( URLRequest(url: youtubeURL) )
    }
}
