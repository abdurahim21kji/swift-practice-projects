//
//  SubscriptionViewController.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/31.
//

import UIKit

class SubscriptionViewController: UIViewController, UICollectionViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionVIew.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProProfileCollectionViewCell", for: indexPath) as? ProProfileCollectionViewCell else { return UICollectionViewCell()}
        cell.profileName.text = "조준"
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell horizonatally
        return 1000
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell vertically
        return 20000
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
//        // give space top left bottom and right for cells
//        return UIEdgeInsets(top: 0, left: 100, bottom: 100, right: 100)
//    }

    @IBOutlet weak var collectionVIew: UICollectionView!
}

class ProProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBAction func subscriptionButtonDidTapped(_ sender: Any) {
        print("subscribed")
    }
}
 
