//
//  TodoManager.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/23.
//

import Foundation

class TodoManager {
    static let shared = TodoManager()
    
    var todos: [Todo] = []
    var numberOfCompletedWorkout: Int = 0
    
    func createTodo(contents: String, count: Int){
        for _ in 0..<count {
            self.todos.append(Todo(contents: contents, count: count, isCompleted: false))
        }
        Storage.store(TodoManager.shared.todos, to: .documents, as: "todos.json")
    }
    
    func increaseNumberOfCompletedWorkout(){
        self.numberOfCompletedWorkout = self.numberOfCompletedWorkout + 1
    }
    
    func decreaseNumberOfCompletedWorkout(){
        self.numberOfCompletedWorkout = self.numberOfCompletedWorkout - 1
    }
}
