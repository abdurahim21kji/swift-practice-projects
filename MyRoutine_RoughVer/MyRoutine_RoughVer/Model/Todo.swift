//
//  Todo.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/22.
//

struct Todo: Codable {
    let contents: String
    let count: Int
    var isCompleted: Bool
    
    init(contents: String, count: Int, isCompleted: Bool) {
        self.contents = contents
        self.count = count
        self.isCompleted = isCompleted
    }
}
