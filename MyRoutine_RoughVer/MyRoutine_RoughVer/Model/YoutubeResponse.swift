//
//  youtubeRequestedModel.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit

struct YoutubeResponse: Decodable {
    let kind: String
    let nextPageToken: String
    let regionCode: String
    
    let pageInfo: PageInfo
    let items: [Item]
}

struct PageInfo: Decodable {
    var totalResults = 0
    var resultsPerPage = 0
}

struct Item: Decodable {
    var kind = ""
    var etag = ""
    var id: Id
    //var snippet: Snippet
}

struct Id: Decodable {
    var kind = ""
    var videoId: String?
}

struct Snippet: Decodable {
    var channelId = ""
    var title = ""
    var description = ""
    var channelTitle = ""
    var thumbnails: Thumbnail
}

struct Thumbnail: Decodable {
    var medium: ChannelURL
    var high: ChannelURL
}

struct ChannelURL: Decodable {
    var url = ""
}
