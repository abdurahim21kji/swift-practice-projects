//
//  Request.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/27.
//

import UIKit

struct Requester {
    
    let key = "AIzaSyBdpe9tBJwS1UPOBAOxsm7ycFGDfpcmtnY"
    
    func request(contents: String ,completionHandler: @escaping (YoutubeResponse)->Void) {
        guard let contents = contents.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        //guard let url = URL(string: "https://www.googleapis.com/youtube/v3/search?part=snippet&q=\(contents)&key=\(self.key)") else {
        //    return }
        guard let url = URL(string: "https://www.googleapis.com/youtube/v3/search?q=\(contents)&key=\(self.key)") else {
            return }
        let urlRequest = URLRequest(url: url)
        print(url)
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let error = error { print("error: \(error)"); return }
            
            if let response = response {print("response: \(response)") }
            
            if let data = data {
                do {
                    let response = try JSONDecoder().decode(YoutubeResponse.self, from: data)
                    completionHandler(response)
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
}
