//
//  SpecificWorkOutListCell.swift
//  MyRoutine_RoughVer
//
//  Created by 김윤석 on 2021/03/25.
//

import UIKit

class SpecificWorkOutListCell: UITableViewCell {
    @IBOutlet weak var workOut: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    @IBAction func playButtonDidTapped(_ sender: Any) {
        playButtonTapHandler?()
    }
    var playButtonTapHandler: (() -> Void)?
}
