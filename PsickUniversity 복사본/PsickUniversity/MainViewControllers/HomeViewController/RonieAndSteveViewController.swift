//
//  RonieAndSteveViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import Foundation
import UIKit

class RonieAndSteveViewController: UIViewController {
    
    @IBOutlet weak var RonieAndSteveCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RonieAndSteveCollectionView.delegate = self
        RonieAndSteveCollectionView.dataSource = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            print(Manager.shared.hanSarangItems.count)
            self.RonieAndSteveCollectionView.reloadData()
        }
    }
}

extension RonieAndSteveViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Manager.shared.ronieAndSteveItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RonieAndSteveThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}

        let url = URL(string: Manager.shared.ronieAndSteveItems[indexPath.item].snippet.thumbnails.medium.url)
        
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
    
    
}

extension RonieAndSteveViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did tapped")
    }
}
