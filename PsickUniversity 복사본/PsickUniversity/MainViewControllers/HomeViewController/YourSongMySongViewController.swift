//
//  YourSongMySongViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/01.
//

import UIKit

class YourSongMySongViewController: UIViewController {

    @IBOutlet weak var yourSongMySongCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yourSongMySongCollectionView.dataSource = self
        yourSongMySongCollectionView.delegate = self

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            print(Manager.shared.hanSarangItems.count)
            self.yourSongMySongCollectionView.reloadData()
        }
    }

}

extension YourSongMySongViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Manager.shared.yourSongMySongItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YourSongMySongThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}

        let url = URL(string: Manager.shared.yourSongMySongItems[indexPath.item].snippet.thumbnails.medium.url)
        
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
    
    
}

extension YourSongMySongViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did tapped")
    }
}
