//
//  HanSarangMountainViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import UIKit

//https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvgpart=snippet,id&order=date"

//https://youtube.com/playlist?list=PL1nP78IpsXsMkhTupzGC1QcHb4eJZQ-PY
//playlist id: PL1nP78IpsXsMkhTupzGC1QcHb4eJZQ-PY

class HanSarangMountainViewController: UIViewController {
    
    @IBOutlet weak var hanSarangMountainCollectionView: UICollectionView!
    
    let urlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsMkhTupzGC1QcHb4eJZQ-PY&part=snippet,id&order=date&maxResults=58"
    
    let hanSarangUrlString = "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsMkhTupzGC1QcHb4eJZQ-PY&part=snippet,id&orderby=rating&maxResults=5"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hanSarangMountainCollectionView.dataSource = self
        hanSarangMountainCollectionView.delegate = self
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //            print(Manager.shared.hanSarangItems.count)
        //            self.hanSarangMountainCollectionView.reloadData()
        //        }
        Service.shared.fetchSpecificPlayList(searchTerm: hanSarangUrlString) {[weak self] items, nextPageToken, error in
            DispatchQueue.main.async {
                
                Manager.shared.hanSarangItems = items
                self?.hanSarangMountainCollectionView.reloadData()
            }
        }
    }
}

extension HanSarangMountainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Manager.shared.hanSarangItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HanSarangMountainThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        let item = Manager.shared.hanSarangItems[indexPath.item]
        let url = URL(string: item.snippet.thumbnails.maxres?.url ?? item.snippet.thumbnails.medium.url )
        
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
}

extension HanSarangMountainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .fullScreen
        //vc.youtubeVideoId = Manager.shared.bDateItems[indexPath.item].id.videoId
        vc.youtubeVideoId = Manager.shared.hanSarangItems[indexPath.item].snippet.resourceId?.videoId
        present(vc, animated: true, completion: nil)
    }
}
