//
//  RonieAndSteveViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import Foundation
import UIKit

class RonieAndSteveViewController: UIViewController {
    
    @IBOutlet weak var RonieAndSteveCollectionView: UICollectionView!
    
    var nextPageToken: String?
    
    let ronieAndSteveUrlString =
        "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PL1nP78IpsXsN22vkofUUoR_KIezgch7_K&part=snippet,id&orderby=rating&maxResults=5"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RonieAndSteveCollectionView.delegate = self
        RonieAndSteveCollectionView.dataSource = self
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //            print(Manager.shared.hanSarangItems.count)
        //            self.RonieAndSteveCollectionView.reloadData()
        //        }
        
        Service.shared.fetchSpecificPlayList(searchTerm: ronieAndSteveUrlString) { [weak self] items, nextPageToken, error in
            
            DispatchQueue.main.async {
                
                Manager.shared.ronieAndSteveItems = items
                self?.nextPageToken = nextPageToken
                self?.RonieAndSteveCollectionView.reloadData()
            }
        }
    }
}

extension RonieAndSteveViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Manager.shared.ronieAndSteveItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RonieAndSteveThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}
        
        let item = Manager.shared.ronieAndSteveItems[indexPath.item]
        let url = URL(string: item.snippet.thumbnails.maxres?.url ?? item.snippet.thumbnails.medium.url )
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.x
        
        if position > RonieAndSteveCollectionView.contentSize.width - scrollView.frame.size.width + 70 {
            guard let nextPageToken = nextPageToken,
                !Service.shared.isPaginating else { return }
            //print( bDateUrlString + "&pageToken=\(nextPageToken)")
            
           // guard !Service.shared.isPaginating else {return }
            
            Service.shared.fetchSpecificPlayList(paginating: true, searchTerm: ronieAndSteveUrlString + "&pageToken=\(nextPageToken)") { items, nextPageToken, error in
                self.nextPageToken = nextPageToken
                print(items)
                let endIndex = Manager.shared.bDateItems.endIndex
                Manager.shared.ronieAndSteveItems.append(contentsOf: items)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.RonieAndSteveCollectionView.reloadData()
                    //self.bDateCollectionView.scrollToItem(at: .init(index: 0), at: .centeredHorizontally, animated: true)
//                    print("count: \(self.count)")
//                    self.count += 1
                }
            }
        }
    }
}

extension RonieAndSteveViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .fullScreen
        //vc.youtubeVideoId = Manager.shared.bDateItems[indexPath.item].id.videoId
        vc.youtubeVideoId = Manager.shared.ronieAndSteveItems[indexPath.item].snippet.resourceId?.videoId
        present(vc, animated: true, completion: nil)
    }
}
