//
//  YourSongMySongViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/01.
//

import UIKit

class YourSongMySongViewController: UIViewController {

    @IBOutlet weak var yourSongMySongCollectionView: UICollectionView!
    
    let yourSongMySongUrlString =
        "https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&playlistId=PLRmKGEHn3NPqIlSAEkS1h0PDOuoc5VoP8&part=snippet,id&orderby=rating&maxResults=5"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yourSongMySongCollectionView.dataSource = self
        yourSongMySongCollectionView.delegate = self

//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            print(Manager.shared.hanSarangItems.count)
//            self.yourSongMySongCollectionView.reloadData()
//        }
        
        Service.shared.fetchSpecificPlayList(searchTerm: yourSongMySongUrlString) { [weak self] items, nextPageToken, error in
            DispatchQueue.main.async {
                
                Manager.shared.yourSongMySongItems = items
                self?.yourSongMySongCollectionView.reloadData()
            }
        }
    }
}

extension YourSongMySongViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return Manager.shared.yourSongMySongItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YourSongMySongThumbNailCell", for: indexPath) as? VideoThumbNailCell else {return UICollectionViewCell()}

        let item = Manager.shared.yourSongMySongItems[indexPath.item]
        let url = URL(string: item.snippet.thumbnails.maxres?.url ?? item.snippet.thumbnails.medium.url )
        cell.thumbnail.sd_setImage(with: url, completed: nil)
        return cell
    }
    
    
}

extension YourSongMySongViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Player", bundle: nil).instantiateViewController(identifier: "YoutubePlayerViewController") as? YoutubePlayerViewController else {return }
        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .fullScreen
        //vc.youtubeVideoId = Manager.shared.bDateItems[indexPath.item].id.videoId
        vc.youtubeVideoId = Manager.shared.yourSongMySongItems[indexPath.item].snippet.resourceId?.videoId
        present(vc, animated: true, completion: nil)
    }
}
