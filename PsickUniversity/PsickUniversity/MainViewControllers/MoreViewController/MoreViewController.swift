//
//  MoreViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/23.
//

import UIKit

class MoreViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MoreViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
        }
        else if indexPath.row == 1{
            
        }
        else if indexPath.row == 2{
            guard let aboutViewController = storyboard?.instantiateViewController(identifier: "AboutViewController") else {return}
            navigationController?.pushViewController(aboutViewController, animated: true)
        }
    }
}
