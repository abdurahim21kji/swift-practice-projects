//
//  SearchViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/23.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
    }

}

extension SearchViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Service2.shared.fetchApps(searchTerm: searchText) { items, error in
            print(items)
        }
    }
}
