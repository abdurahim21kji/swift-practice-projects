//
//  Model.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/04/30.
//

import Foundation

struct SpecificPlayListService: Decodable {
    let kind: String
    let etag: String
    let nextPageToken: String
    let prevPageToken: String?
    let region: String?
    var items: [Item]
    let pageInfo: PageInfo?
    
//    enum CodingKeys: String, CodingKey {
//        case kind
//        case etag
//        case nextPageToken
//        case region
//        case items
//        case pageInfo
//    }
    
//    required init(from decoder: Decoder) throws {
////         let values = try decoder.container(keyedBy: CodingKeys.self)
////         order_type = try values.decode(Int.self, forKey: .orderType)
////         movies = try values.decode([Movie].self, forKey: .movies)
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        kind = try values.decode(String.self, forKey: .kind)
//        etag = try values.decode(String.self, forKey: .kind)
//        nextPageToken = try values.decode(String.self, forKey: .kind)
//        region = try values.decode(String.self, forKey: .kind)
//        items = try values.decode([Item].self, forKey: .items)
//        pageInfo = try values.decode(PageInfo.self, forKey: .pageInfo)
//     }
}

//"kind": "youtube#playlistItemListResponse",
//  "etag": "ayQqm7XObTrYmvZsS1becrniXPY",
//  "nextPageToken": "CAoQAA",
//  "prevPageToken": "CAUQAQ",
//  "items": [

struct Item: Decodable {
    let kind: String
    let etag: String
    let id: String?
    let snippet: Snippet
}
//
//struct Snippet: Decodable {
//    var publishedAt = ""
//    var channelId = ""
//    var title = ""
//    var description = ""
//    var thumbnails: Thumbnail
//    var channelTitle = ""
//    var playlistId = ""
//    var position: Int
//    var resourceId: ResourceId
//    var videoOwnerChannelTitle = ""
//    var videoOwnerChannelId = ""
//}


struct Snippet: Decodable {
    var publishedAt = ""
    var channelId = ""
    var title = ""
    var description = ""
    var thumbnails: Thumbnail
    var channelTitle: String?
    var playlistId: String?
    var position: Int?
    var resourceId: ResourceId?
    var videoOwnerChannelTitle: String?
    var videoOwnerChannelId: String?
    
    var liveBroadcastContent: String?
    var publishTime: String?
}


struct Thumbnail: Decodable {
    var defaultOne: ChannelURL
    var medium: ChannelURL
    var high: ChannelURL
    var standard: ChannelURL?
    var maxres: ChannelURL?

    enum CodingKeys: String, CodingKey {
        case defaultOne = "default"
        case medium
        case high
        case standard
        case maxres
    }
}


struct ChannelURL: Decodable {
    var url = ""
    var width: Int
    var height: Int
}


struct ResourceId: Decodable{
    var kind: String
    var videoId: String
}

struct PageInfo: Decodable{
    var totalResults: Int
    var resultsPerPage: Int
}


// "items": [
//   {
//     "id": {
//       "kind": "youtube#video",
//       "videoId": "jyJp46-vOPM"
//     },
//     "snippet": {
//       "publishedAt": "2021-05-01T10:00:16Z",
//       "channelId": "UCGX5sP4ehBkihHwt5bs5wvg",
//       "title": "[B대면데이트]#6 여섯번째 데이트 최준/35/카페사장",
//       "description": "피식대학\u200b#B대면데이트\u200b#최준.",
//       "thumbnails": {
//         "default": {
//           "url": "https://i.ytimg.com/vi/jyJp46-vOPM/default.jpg",
//           "width": 120,
//           "height": 90
//         },
//         "medium": {
//           "url": "https://i.ytimg.com/vi/jyJp46-vOPM/mqdefault.jpg",
//           "width": 320,
//           "height": 180
//         },
//         "high": {
//           "url": "https://i.ytimg.com/vi/jyJp46-vOPM/hqdefault.jpg",
//           "width": 480,
//           "height": 360
//         }
//       },
//       "channelTitle": "피식대학Psick Univ",
//       "liveBroadcastContent": "none",
//       "publishTime": "2021-05-01T10:00:16Z"
//     }
//   },



//    {
//      "kind": "youtube#playlistItem",
//      "etag": "1XKW58CpUn4uNwDwgpxcbrGo7as",
//      "id": "UEwxblA3OElwc1hzUEt6NGhWWGhxNHdSSksza0tnSlNkcC4xMkVGQjNCMUM1N0RFNEUx",
//      "snippet": {
//        "publishedAt": "2020-12-24T12:59:50Z",
//        "channelId": "UCGX5sP4ehBkihHwt5bs5wvg",
//        "title": "[B대면데이트]#2. 두번째 데이트 방재호/33/다단계",
//        "description": "#B대면데이트 #두번째만남 #썸\n\n방재호 instagram@BJH_Dreamlife",
//        "thumbnails": {
//          "default": {
//            "url": "https://i.ytimg.com/vi/HWx1mN5V8uU/default.jpg",
//            "width": 120,
//            "height": 90
//          },
//          "medium": {
//            "url": "https://i.ytimg.com/vi/HWx1mN5V8uU/mqdefault.jpg",
//            "width": 320,
//            "height": 180
//          },
//          "high": {
//            "url": "https://i.ytimg.com/vi/HWx1mN5V8uU/hqdefault.jpg",
//            "width": 480,
//            "height": 360
//          },
//          "standard": {
//            "url": "https://i.ytimg.com/vi/HWx1mN5V8uU/sddefault.jpg",
//            "width": 640,
//            "height": 480
//          },
//          "maxres": {
//            "url": "https://i.ytimg.com/vi/HWx1mN5V8uU/maxresdefault.jpg",
//            "width": 1280,
//            "height": 720
//          }
//        },
//        "channelTitle": "피식대학Psick Univ",
//        "playlistId": "PL1nP78IpsXsPKz4hVXhq4wRJK3kKgJSdp",
//        "position": 5,
//        "resourceId": {
//          "kind": "youtube#video",
//          "videoId": "HWx1mN5V8uU"
//        },
//        "videoOwnerChannelTitle": "피식대학Psick Univ",
//        "videoOwnerChannelId": "UCGX5sP4ehBkihHwt5bs5wvg"
//      }
//    }
