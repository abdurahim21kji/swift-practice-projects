//
//  Requester.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/02.
//

import UIKit

protocol Requester {
    associatedtype ResponseType: Decodable
}

extension Requester {
    func fetchApps(searchTerm: String, completion: @escaping (ResponseType?)->Void) {
        
        //let urlString = "https://www.googleapis.com/youtube/v3/playlists?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvgpart=snippet,id&order=date"
       
        guard let url = URL(string: searchTerm) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                //completion([], nil)
                return
            }
            
            guard let data = data else {return}
            
            do {
                let searchResult = try JSONDecoder().decode(ResponseType.self, from: data)
                
                completion(searchResult)
                
            } catch let Jsonerror {
                //completion([], Jsonerror)
                print(Jsonerror.localizedDescription)
                print(Jsonerror)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
//
//struct SpecificPlaylistFromChannel: Requester{
//    typealias ResponseType = <#type#>
//}
//
//struct AllListFromChannel: Requester {
//    typealias ResponseType = <#type#>
//}



class Service2{
    static let shared = Service2()
    
    func fetchApps(searchTerm: String, completion: @escaping ([Item2], Error?)->()){
        //let urlString = "http://itunes.apple.com/search?term=\(searchTerm)&entity=software"
        
        //https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=50
        //qzXJckVxE4w
        
        //https://www.googleapis.com/youtube/v3/search?key={your_key_here}&channelId={channel_id_here}&part=snippet,id&order=date&maxResults=20
        
//        let urlString = "https://www.googleapis.com/youtube/v3/search?key=\(searchTerm)&channelId=IOSAcademy&part=snippet,id&order=date&maxResults=20"
//        https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&video=Swift&channelId=IOSAcademy&part=snippet,id&order=date&maxResults=20
//
        let urlString = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=\(searchTerm)&maxResults=50&order=viewCount&key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg"
        
        //모델
        
//https://www.googleapis.com/youtube/v3/search?part=snippet&q=모델&maxResults=50&order=viewCount&key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg
        
//https://www.googleapis.com/youtube/v3/search?part=snippet&q=황희&maxResults=50&order=viewCount&key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg
        
//https://www.googleapis.com/youtube/v3/search?part=snippet&q=%ED%99%A9%ED%9D%AC&maxResults=50&order=viewCount&key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg
        
        guard let target = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            print("Error")
            return }
        
        print(target)
        
        guard let url = URL(string: target) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let err = error {
                print("Failed to fetch apps:", err)
                completion([], nil)
                return
            }
            
            //success
            //            print(data)
            //            print(String(data: data!, encoding: .utf8))
            guard let data = data else {return}
            do {
                let searchResult = try JSONDecoder().decode(AllPlayListService.self, from: data)
                //print(searchResult.items.count)
                //self.appResult = searchResult.results
                //                DispatchQueue.main.async {
                //                    self.collectionView.reloadData()
                //                }
                completion(searchResult.items, nil)
                
            } catch let Jsonerror {
                completion([], Jsonerror)
                print(Jsonerror)
                print(Jsonerror.localizedDescription)
                print("Failed To Decode")
            }
            
        }.resume()
    }
}
