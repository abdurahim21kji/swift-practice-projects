//
//  YoutubePlayerViewController.swift
//  PsickUniversity
//
//  Created by 김윤석 on 2021/05/26.
//

import Foundation
import youtube_ios_player_helper

class YoutubePlayerViewController: UIViewController, YTPlayerViewDelegate {
    
    //    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    //        return .landscapeRight
    //    }
    
    @IBOutlet var playerView: YTPlayerView!
    
    @IBOutlet var playButton: UIButton!
    
    @IBAction func playButtonDidTapped() {
        playButton.isSelected = true
        
        playerView.playVideo()
    }
    
    @IBOutlet var closeButton: UIButton!
    
    @IBAction func closeButtonDidTapped(){
        print("Did tapped")
    }
    
    var youtubeVideoId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerView.delegate = self
        
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        guard let id = youtubeVideoId else { return }
        playerView.load(withVideoId: id)
        
        playerView.stopVideo()
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        print(state)
        
        if playButton.isSelected {
            switch state {
            case .playing:
                playerView.playVideo()
            default:
                break
            }
        }
        
        //        switch state {
        //        case .ended:
        //            dismiss(animated: true, completion: nil)
        //
        //        case .unstarted:
        //            print("unstarted")
        //            break
        //
        //        case .paused:
        //            print("paused")
        //            playerView.stopVideo()
        //            dismiss(animated: true, completion: nil)
        //            break
        //
        //        case .playing:
        //            print("Playing")
        //            break
        //
        //        default: break
        //
        //        }
    }
    
    
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
}

import AVKit
import WebKit

class WebKitView: WKWebView {
    init(frame: CGRect, videoId: String){
        super.init(frame: frame, configuration: WKWebViewConfiguration())
        
        backgroundColor = .red
        if let url = URL(string:"https://www.youtube.com/embed/\(videoId)" ) {
            let request:URLRequest = URLRequest(url: url)
            self.load(request)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class VideoPlayerView: UIView {
    init(frame: CGRect, videoId: String) {
        super.init(frame: frame)
        
        backgroundColor = .black
 //       "https://www.youtube.com/embed/\(videoID)"
        if let url = URL(string:"https://www.youtube.com/embed/\(videoId)" ) {
            let player = AVPlayer(url: url)
            
            let playerLayer = AVPlayerLayer(player: player)
            self.layer.addSublayer(playerLayer)
            player.play()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}

class VideoLauncher: NSObject {
    
    var videoId: String?
    
    func showVideoPlayer(){
        if let keyWindow = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
            let view = UIView(frame: keyWindow.frame)
            view.backgroundColor = .blue
            
            view.frame = CGRect(x: keyWindow.frame.width - 10, y: keyWindow.frame.height - 10, width: 10, height: 10)
            
            let videoPlayerViewFrame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: keyWindow.frame.width * 9/16)
//            let videoPlayerView = VideoPlayerView(frame: videoPlayerViewFrame, videoId: videoId ?? "")
//
//            print(videoId)
//
//            view.addSubview(videoPlayerView)
            
            let videoWebPlayerView = WebKitView(frame: videoPlayerViewFrame, videoId: videoId ?? "")

            view.addSubview(videoWebPlayerView)
            
            keyWindow.addSubview(view)
            
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut) {
                view.frame = keyWindow.frame
            } completion: { completedAnimation in
                
            }
        }
    }
}
