//
//  BaseTabBarViewController.swift
//  PsickUniversityPragmmatically
//
//  Created by 김윤석 on 2021/04/30.
//

import UIKit

class BaseTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [
            createNavigationViewController(viewController: HomeViewController(), title: "Home", tabBarImage: "house", selectedTabBarImage: "house.fill"),
            createNavigationViewController(viewController: SearchViewController(), title: "Search", tabBarImage: "magnifyingglass", selectedTabBarImage: "magnifyingglass"),
            createNavigationViewController(viewController: FavoriteViewController(), title: "Favorite", tabBarImage: "star", selectedTabBarImage: "star.fill"),
            createNavigationViewController(viewController: MoreViewController(), title: "More", tabBarImage: "gearshape", selectedTabBarImage: "gearshape.fill")
        ]
    }
    
    private func createNavigationViewController(viewController: UIViewController, title: String, tabBarImage: String, selectedTabBarImage: String) -> UIViewController {
        
        let navigationViewController = UINavigationController(rootViewController: viewController)
        navigationViewController.tabBarItem.title = title
        navigationViewController.tabBarItem.image = UIImage(systemName: tabBarImage)
        navigationViewController.tabBarItem.selectedImage = UIImage(systemName: selectedTabBarImage)
        return navigationViewController
    }

}
