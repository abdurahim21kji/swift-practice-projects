//
//  HomeViewController.swift
//  PsickUniversityPragmmatically
//
//  Created by 김윤석 on 2021/04/30.
//


// psick univ id: UCGX5sP4ehBkihHwt5bs5wvg
//key : AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28

//https://www.googleapis.com/youtube/v3/search?key=AIzaSyCw3TImiz7vnZOF4pYF3onowryMg-68H28&channelId=UCGX5sP4ehBkihHwt5bs5wvg&part=snippet,id&order=date&maxResults=20

import UIKit

class HomeViewController: UIViewController {

    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .gray
        return scrollView
    }()
    
    let titleImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .red
        return image
    }()
    
    let stackView1: UIStackView = {
        let scrollView = UIStackView()
        scrollView.backgroundColor = .blue
        
        return scrollView
    }()
    
    let stackView2: UIStackView = {
        let scrollView = UIStackView()
        scrollView.backgroundColor = .green
        
        return scrollView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .yellow
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        titleImage.translatesAutoresizingMaskIntoConstraints = false
        stackView1.translatesAutoresizingMaskIntoConstraints = false
        stackView2.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        
        
        stackView2.frame = CGRect(x: 20, y: 2000, width: 100, height: 100)
        
        scrollView.addSubview(titleImage)
        scrollView.addSubview(stackView1)
        scrollView.addSubview(stackView2)
        
        scrollView.contentSize = CGSize(width: view.frame.size.width, height: 2200)
        scrollView.frame = CGRect(x: 10, y: 10, width: view.frame.size.width - 20, height: view.frame.size.height - 20)
        //scrollView.frame = view.bounds
//        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
//        scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
//        scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
//        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        titleImage.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        titleImage.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        titleImage.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0).isActive = true
        titleImage.heightAnchor.constraint(equalTo: titleImage.widthAnchor, multiplier: 0.5).isActive = true
        titleImage.widthAnchor.constraint(equalToConstant: scrollView.bounds.width).isActive = true
        
        stackView1.topAnchor.constraint(equalTo: titleImage.bottomAnchor, constant: 8).isActive = true
        stackView1.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        stackView1.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        stackView1.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        stackView1.heightAnchor.constraint(equalToConstant: 400).isActive = true
    }

}
