//
//  ViewController.swift
//  SetupUIProgrammaticallyPractice
//
//  Created by 김윤석 on 2021/06/06.
//

import UIKit

class BaseViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc1 = ViewController1()
        vc1.tabBarItem.title = "vc1"
        
        let vc2 = ViewController2()
        vc2.tabBarItem.title = "vc2"
        
        let vc3 = ViewController3()
        vc3.tabBarItem.title = "vc3"
        
        viewControllers = [vc1, vc2, vc3]
        
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.right"), style: .plain, target: self, action: #selector(buttonHandler))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        setupUI()
    }
    
    @objc func buttonHandler() {
        let vc = ViewController1()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI() {
        navigationController?.navigationBar.isTranslucent = true
        view.backgroundColor = .red
        navigationController?.navigationBar.barTintColor = .cyan
        navigationItem.title = "Main"
    }
}
