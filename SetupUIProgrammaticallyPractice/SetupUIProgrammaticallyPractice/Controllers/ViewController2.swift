//
//  ViewController2.swift
//  SetupUIProgrammaticallyPractice
//
//  Created by 김윤석 on 2021/06/06.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.right"), style: .plain, target: self, action: #selector(buttonHandler))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        setupUI()
    }
    
    @objc func buttonHandler(){
        let vc = ViewController3()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI(){
        view.backgroundColor = .blue
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "VC 2"
        navigationController?.navigationBar.barTintColor = .systemPink
    }
}
