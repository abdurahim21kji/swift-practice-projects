//
//  TableViewDelegate.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

import UIKit


extension DetailMovieViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentListCell", for: indexPath) as? CommentListCell else {return UITableViewCell()}
        
        cell.writerLabel.text = movieComments[indexPath.row].writer
        cell.timestampLabel.text = String(describing: Date(timeIntervalSince1970: movieComments[indexPath.row].timestamp))
        cell.contentsLabel.text = movieComments[indexPath.row].contents
        return cell
    }
}
