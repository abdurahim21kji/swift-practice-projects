//
//  DetailMovieView.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/02/28.
//

import UIKit

class DetailMovieViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.requestDetailInformationOfTheMovie()
        self.requestCommentsOfTheMovie()
    }
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    @IBOutlet weak var synopsisTextField: UITextView!
    
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var actorsLabel: UILabel!
    
    @IBOutlet weak var reservationRateLabel: UILabel!
    @IBOutlet weak var userRatingLabel: UILabel!
    @IBOutlet weak var audienceLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!

    @IBAction func writeCommentButtonWasTapped(_ sender: Any) {
        if let writeCommentViewController = self.storyboard?.instantiateViewController(identifier: "WriteComment") as? WriteCommentViewController {
            writeCommentViewController.delegate = self
            writeCommentViewController.movieId = self.movieId
            self.navigationController?.present(writeCommentViewController, animated: true)
        }
    }
    
    var movieComments = [MovieComment]()
    
    var movieId: String = ""
}

extension DetailMovieViewController: WriteCommentViewControllerDelegate {
    func cancelButtonWasTapped(_ writeCommentViewController: WriteCommentViewController) {
        self.navigationController?.dismiss(animated: true)
    }
    
    func completeButtonWasTapped(_ writeCommentViewController: WriteCommentViewController) {
        self.requestCommentsOfTheMovie()
        self.navigationController?.dismiss(animated: true)
    }
}

private extension DetailMovieViewController {
    func updateValuesToViews(_ movieDetail: MovieDetail) {
        guard let url = URL(string: movieDetail.image) else { return }
        self.downloadImage(from: url)
        
        self.titleLabel.text = movieDetail.title
        self.navigationItem.title = movieDetail.title
        
        self.dateLabel.text = movieDetail.date
        self.genreLabel.text = movieDetail.genre
        self.durationLabel.text = String(describing: movieDetail.duration)
        
        self.reservationRateLabel.text = String(describing: movieDetail.reservationRate)
        self.userRatingLabel.text = String(describing: movieDetail.userRating)
        self.audienceLabel.text = String(describing: movieDetail.audience)
        
        self.synopsisTextField.text = movieDetail.synopsis
        
        self.directorLabel.text = movieDetail.director
        self.actorsLabel.text = movieDetail.actor
    }
    
    func downloadImage(from url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.movieImage.image = UIImage(data: data)
            }
        }.resume()
    }
    
    func requestDetailInformationOfTheMovie() {
        if let url = BoxOfficeURLConfigurationMaker(path: "movie", query: ["id": self.movieId]).url{
            let urlRequest = URLRequest(url: url)
            
            MovieDetailRequester().request(with: urlRequest){ [weak self] movieDetail in
                DispatchQueue.main.async {
                    guard let movieDetail = movieDetail else {return }
                    self?.updateValuesToViews(movieDetail)
                }
            }
        }
    }
    
    func requestCommentsOfTheMovie() {
        if let url = BoxOfficeURLConfigurationMaker(path: "comments", query: ["movie_id" : self.movieId]).url{
            let urlRequest = URLRequest(url: url)
            
            MovieCommentRequester().request(with: urlRequest){ [weak self] response in
                DispatchQueue.main.async {
                    guard let response = response else { return }
                    self?.movieComments = response.comments
                    self?.tableView.reloadData()
                }
            }
        }
    }
}
