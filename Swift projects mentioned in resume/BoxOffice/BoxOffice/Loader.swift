//
//  Loader.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/02/02.
//

import Foundation

class DataLoader {
    
    @Published var userData = [UserData]()
    
    init(){
        loadFromInternet()
    }
    
    func loadFromInternet(){
        if let url = URL(string: "http://connect-boxoffice.run.goorm.io/movies"){
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if let data = data {
                    do{
                        let jsonDecoder = JSONDecoder()
                        let dataFromJson = try jsonDecoder.decode(Response1.self, from: data)
                        
                        print(data)
                    }
                    catch let error {
                        print(error)
                    }
                }
                
            }.resume()
        }
    }
}

struct Response1: Codable{
    let order_type : Int
    let movies : [Movie]
}

struct UserData: Codable{
    
}
