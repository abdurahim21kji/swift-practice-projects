//
//  Movie.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/01.
//

struct MovieResponse: Decodable {
    let order_type : Int
    let movies : [Movie]
    
    enum CodingKeys: String, CodingKey {
        case orderType = "order_type"
        case movies
    }
    
   init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        order_type = try values.decode(Int.self, forKey: .orderType)
        movies = try values.decode([Movie].self, forKey: .movies)
    }
}

struct Movie: Decodable {
    let grade: Int
    let thumb: String
    let reservationGrade: Int
    let title: String
    let reservationRate: Float
    let userRating: Float
    let date: String
    let id: String
    
    
    enum CodingKeys: String, CodingKey {
        case grade
        case thumb
        case reservationGrade = "reservation_grade"
        case title
        case reservationRate = "reservation_rate"
        case userRating = "user_rating"
        case date
        case id
    }
}
