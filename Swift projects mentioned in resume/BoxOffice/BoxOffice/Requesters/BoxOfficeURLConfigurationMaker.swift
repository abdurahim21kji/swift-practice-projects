//
//  Configuration.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/18.
//

import Foundation

struct BoxOfficeURLConfigurationMaker {
    private var baseURL: String = "http://connect-boxoffice.run.goorm.io/"
    var url: URL? = URL(string: "")
    
    init (path: String, query: [String: String]) {
        self.baseURL.append(path)
        if let url = URL(string: baseURL) {
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
            urlComponents?.queryItems = query.map { URLQueryItem(name: $0.key, value: $0.value) }
            self.url = (urlComponents?.url)
        }
    }
}
