//
//  MovieDetailRequester.swift
//  BoxOffice
//
//  Created by 김윤석 on 2021/03/10.
//

struct MovieDetailRequester: Requester{

    typealias ResponseType = MovieDetail

}
