//
//  DataLoader.swift
//  WeatherTableView
//
//  Created by 김윤석 on 2021/01/22.
//

import Foundation
public class CountryDataLoader {
    @Published var userData = [UserData]()
    
    //var myData = [MyData]()
    
    init() {
        //loadFromInternet()

        load()
    }
    
    func loadFromInternet() {
        if let url = URL(string: "https://jsonplaceholder.typicode.com/posts") {
            URLSession.shared.dataTask(with: url){ (data, response, error) in
                if let data = data{
                    do {
                        let jsonDecoder = JSONDecoder()
                        // let dataFromJson = try jsonDecoder.decode(MyData.self, from: data)
                        let dataFromJson = try jsonDecoder.decode([MyData].self, from: data)
                        
                        print(dataFromJson)
                    }
                    catch let error {
                        print(error)
                    }
                }
            }.resume()
        }
    }
    
    func load(){
        if let fileLocation = Bundle.main.url(forResource: "assets/countries.dataset/countries", withExtension: "json"){
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([UserData].self, from: data)
                self.userData = dataFromJson
            } catch {
                print(error)
            }
        }
    }
}

struct UserData: Codable {
    
    var korean_name : String
    var asset_name : String
}

struct MyData: Codable {
    
    
    var userId: Int
    var id : Int
    var title : String
    var body : String
//    var reservation_rate : Double
//    var id : String
//    var thumb : String
//    var date : String
//    var title : String
//    var reservation_grade : Int
//    var user_rating : Double
//    var grade : Int
    
}
