//
//  ViewController.swift
//  WeatherTableView
//
//  Created by 김윤석 on 2021/01/21.
//

import UIKit

class WorldWeatherViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CountryWeatherViewControllerDelegate {
    
    let data = CountryDataLoader().userData
   
    //let myData = CountryDataLoader().myData

    @IBOutlet weak var upperTab: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        upperTab.title = "세계 날씨"
        upperTab.backButtonTitle = "세계 날씨"
        
        print("\(data.count)")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].korean_name
        return cell
    }
    
    //해당 row에 있는 cell을 클릭했을때
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let countryWeatherViewController = self.storyboard?.instantiateViewController(identifier: "CountryWeather") as? CountryWeatherViewController {
            
            countryWeatherViewController.upperTab.title = data[indexPath.row].korean_name
            countryWeatherViewController.upperTab.backButtonTitle = data[indexPath.row].korean_name
            
            let cityData = getCityData(at : indexPath.row)
            
            countryWeatherViewController.data = cityData?.cityData
            countryWeatherViewController.delegate = self
            
            self.navigationController?.pushViewController(countryWeatherViewController, animated: true)
        }
    }
    
    func HomeButtonWasTapped(_ countryWeatherViewController: CountryWeatherViewController) {
        countryWeatherViewController.navigationController?.popViewController(animated: true)
    }
    
    
    private func getCityData(at : Int) -> CityDataLoader? {
        switch(at) {
        case 0:
            return CityDataLoader("assets/kr.dataset/kr")
        case 1:
            return CityDataLoader("assets/de.dataset/de")
        case 2:
            return CityDataLoader("assets/it.dataset/it")
        case 3:
            return CityDataLoader("assets/us.dataset/us")
        case 4:
            return CityDataLoader("assets/fr.dataset/fr")
        case 5:
            return CityDataLoader("assets/jp.dataset/jp")
        default:
            break
        }
        return nil
    }
}
