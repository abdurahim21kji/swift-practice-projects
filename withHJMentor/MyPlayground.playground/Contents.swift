//////import UIKit
//////
//////// convention:
////////      - write code for team
//////// study optional
//////// implementation patterns: 유연함,
//////// modern laguage에 관해서
//////// 코드의 가장 큰 문제점:
////////      읽기가 불편하다.
////////      전혀 객체지향적이지 않다.
////////      너무 절차지향적이다.
////////
//////// SRP Single Responsiablity Principle - SOLID의 S다.
////////      가장 지키기 어렵다. 객체 지향적인 원칙이다.
////////      하나의 객체는 하나의 책임만 가져야한다.
////////      객체를 바꿔야한다면, 하나의 이유만 있어야한다.
//////
//////
////////  할것: 1. class나 struct를 써서 책임을 나눠서 구현
////////      2. 100명이 같이 본다고 하고, 나눠준다.
////////      3.  함수는 20, class 300,
//////
//////let balance: Int = 5000
//////let targetNumbers: [Int] = [1,4,11,19,25,31]
//////let targetBonusNumbers: Int = 7
//////
//////var concurrCount3: Int = 0
//////var concurrCount4: Int = 0
//////var concurrCount5: Int = 0
//////var concurrCount51: Int = 0
//////var concurrCount6: Int = 0
//////
//////struct LOT{
//////    let price: Int = 1000
//////    var numbers: [Int] = []
//////}
//////
////////기획서에서 주어진 숫자들
////////나중에 rand로 값을 채워야한다.
//////var lot1: LOT = LOT(numbers: [8,21,23,41,42,43])
//////var lot2: LOT = LOT(numbers: [3,5,11,16,32,38])
//////var lot3: LOT = LOT(numbers: [7,11,16,35,36,44])
//////var lot4: LOT = LOT(numbers: [1,8,11,31,41,42])
//////var lot5: LOT = LOT(numbers: [13,14,16,38,42,45])
//////
//////var lotteries: [LOT] = [lot1, lot2, lot3, lot4, lot5]
//////
////////랜덤하게 값을 채운다.
//////for lot in 0..<lotteries.count
//////{
//////    for number in 0..<6
//////    {
//////        lotteries[lot].numbers[number] = Int(arc4random_uniform(45)) + 1
//////    }
//////}
//////
////////결과 값 보여주기
//////func printResult(lotteries: [LOT])
//////{
//////    for lotteryIndex in 0..<lotteries.count
//////    {
//////        var count = 0
//////        var bonusCount = 0
//////
//////        for numIndex in 0..<lotteries[lotteryIndex].numbers.count
//////        {
//////            for targetNumber in 0..<targetNumbers.count
//////            {
//////                if (lotteries[lotteryIndex].numbers[numIndex] == targetNumbers[targetNumber])
//////                {
//////                    count += 1
//////                }
//////            }
//////
//////            if (lotteries[lotteryIndex].numbers[numIndex] == targetBonusNumbers)
//////            {
//////                bonusCount += 1
//////            }
//////        }
//////
//////        //LOT[]에서는 unable to do index access
//////        if (count == 3){
//////            concurrCount3 += 1
//////        }
//////        else if (count == 4){
//////            concurrCount4 += 1
//////        }
//////        else if (count == 5){
//////            concurrCount5 += 1
//////        }
//////        else if (count == 5 && bonusCount <= 1){
//////            concurrCount51 += 1
//////        }
//////        else if (count == 6){
//////            concurrCount6 += 1
//////        }
//////    }
//////
//////    print("프로그램 실행 결과")
//////    print("로또 구매 금액은 \(balance)원 입니다.")
//////    print("지난주 당첨 번호는 \(targetNumbers)입니다")
//////    print("보너스 볼은 \(targetBonusNumbers)입니다")
//////
//////    print("\(balance/lotteries[0].price)개를 구매했습니다.")
//////    for lottery in 0..<lotteries.count
//////    {
//////        print("\(lotteries[lottery].numbers)")
//////    }
//////
//////    //cal ror
//////    let concurrCount: [Int] = [concurrCount3, concurrCount4, concurrCount5, concurrCount51, concurrCount6]
//////    let prizeList: [Int] = [5000, 50000, 1500000, 30000000, 200000000]
//////    var maxPrize = concurrCount[0] * prizeList[0]
//////
//////    for i in 1..<concurrCount.count
//////    {
//////        if (maxPrize < concurrCount[i] * prizeList[i])
//////        {
//////            maxPrize = concurrCount[i] * prizeList[i]
//////        }
//////    }
//////
//////    var ror: Int
//////    ror = (maxPrize/balance) * 100
//////
//////    print("당첨 통계는 아래와 같습니다.")
//////    print("3개 일치(5000원): \(concurrCount[0])")
//////    print("4개 일치(50,000원): \(concurrCount[1])")
//////    print("5개 일치(1,500,000원): \(concurrCount[2])")
//////    print("5개 일치, 보너스 볼 일치(30,000,000원): \(concurrCount[3])")
//////    print("6개 일치(2,000,000,000): \(concurrCount[4])")
//////    print("총 수익률은 \(ror)입니다.")
//////}
//////
//////printResult(lotteries: lotteries)
////
//////
//////struct Point {
//////    var x = 0, y = 0
//////    func moveToX(x: Int, andY y:Int) { //Needs to be a mutating method in order to work
//////        self.x = x
//////        self.y = y
//////    }
//////}
//////
//////var p = Point(x: 1, y: 2)
//////p.x = 3 //Works from outside the struct!
//////p.moveToX(5, andY: 5)
////
////
////
//////optional
//////var myNumber: Int? = 1
//////var myNumber: String? = "myNumber"
//////
//////var number: [Int] = [1,2,3,4,5]
//////
//////func printFirstLetter(){
//////    guard let firstletter = myNumber?.first else {
//////        print("nil")
//////        return
//////    }
//////    print(firstletter)
//////}
//////
////////var index = myNumber?.remove(at: 2)
//////var removed = number.remove(at: 2)
//////var indexnumber = number.index(number.startIndex, offsetBy: 4)
//////print(removed)
//////print(indexnumber)
//////printFirstLetter()
//////
//////var myName: String? = "Park"
//////var yourName: String? = nil
//////
//////var passedMyName = myName ?? "Kim"
//////var passedYourName = yourName ?? "Lee"
////
//////var multiplyClosure: (Int, Int) -> Int = { a, b in return a * b }
//////
//////print("\(multiplyClosure(1, 2))")
//////
//////func operateTwoNum(a: Int, b: Int, operation: (Int, Int) -> Int) ->Int {
//////    return operation(a, b)
//////}
//////
//////print("\(operateTwoNum(a: 4, b: 2, operation: multiplyClosure))")
//////
//////let voidClosure: () -> Void = {
//////    print("ios development")
//////}
//////
//////voidClosure()
//////
//////var count = 0
//////
//////let incrementer = {
//////    count += 1
//////}
//////
//////incrementer()
//////incrementer()
//////count
////
////
//////{(param) -> return type in
//////    statement
//////}
//////1. cho simple closure
////
//////let chosimpleclosure = {
//////
//////}
//////
//////chosimpleclosure()
//////
////////2. 코드 블럭을 구현한 closure
//////
//////let choSimpleClosure2 = {
//////    print("Hello")
//////}
//////choSimpleClosure2()
//////
////////3. 인풋 파라미터를 받는 closure
//////
//////let choSimpleClosure3: (String) -> Void = { name in
//////    print(name)
//////    dump(name)
//////}
//////
//////choSimpleClosure3("kill")
//////
////////4. 값을 리턴하는 closure
//////
//////let choSimpleClosure4:(String) -> String = { name in
//////    return name + "Love"
//////}
//////
//////print(choSimpleClosure4("Kill"))
//////
////////5. closure를 파라미터로 받는 함수 구현
//////
//////func functionclosure(choSimpleClosure: () -> Void){
//////
//////    print("function")
//////    choSimpleClosure()
//////}
//////
//////functionclosure(choSimpleClosure: {
//////    print("from closure")
//////})
//////
//////
////////Trailing Closure
//////func closurefunction(message: String, choSimpleClosure: () -> Void){
//////    print("\(message)")
//////    choSimpleClosure()
//////}
//////
//////closurefunction(message: "Kim", choSimpleClosure: {
//////    print("from closure")
//////})
//////
//////
//////let names = ["kim", "park", "kong", "Lee"]
//////
//////var sortedNames = names.sorted(by: {(s1: String, s2: String) -> Bool in return s1 > s2 })
//////
//////print(sortedNames)
//////
//////
//////class clLot{
//////    var numbers: [Int] = []
//////}
//////
//////struct stLot{
//////    var numbers: [Int] = []
//////}
//////
//////var cllot = clLot()
//////var stlot = stLot()
//////
//////func changevalues( _ clLot: clLot, _ stLot: stLot){
//////    clLot.numbers.append(1)
//////    stLot.numbers
//////
//////}
//////
//////changevalues(cllot, stlot)
////
////import Foundation
//////
//////struct calculation{
//////    var count = 0
//////    var bonusCount = 0
//////    var b: [Int?] = [nil]
////////    func increase(){
////////        self.count += 1
////////        self.bonusCount += 1
////////    }
//////}
//////
//////var cal = calculation()
//////
//////cal.count += 1
//////cal.bonusCount += 3
//////print(cal.b.first!)
//////print(cal.bonusCount)
////
////
////class Room{
////    var number: Int
////
////    init(number: Int){
////        self.number = number
////    }
////}
////
////class Building {
////    var name: String
////    var room: Room?
////
////    init(name: String){
////        self.name = name
////    }
////}
////
////struct Address {
////    var province: String
////    var city: String
////    var street: String
////    var building: Building?
////    var detailAddress: String?
////
////    init(province: String, city: String, street: String){
////        self.province = province
////        self.city = city
////        self.street = street
////    }
////
//////    func fullAddress() -> String? {
//////        var restAddress: String? = nil
//////
//////        if let buildingInfo: Building = self.building{
//////            restAddress = buildingInfo.name
//////        }else if let
//////    }
////}
////
////class Person{
////    var name: String
////    var address: Address?
////
////    init(name: String){
////        self.name = name
////    }
////}
////
////var yagom: Person = Person(name: "yagom")
////let yagomchaining: Int? = yagom.address?.building?.room?.number
////
//////yagom.address = Address(privince: "seoul", city: "seoul", street: "Seocho gu", building: nil, detailAddress: nil)
//////yagom.address?.building = Building(name: "killDing")
//////yagom.address?.building?.room = Room(number: 123)
//////yagom.address?.building?.room?.number = 500
//////print(yagom.address?.building?.room?.number)
////
////
//
////var numberForName: [String: Int] = [String: Int]()
//
////var numberForName: [String: Int] = [:]
//
////var numberForName: [String: Int] = ["yagom": 100, "chulsoo": 200, "jenny": 300]
////
////print(numberForName.removeValue(forKey: "yagom"))
////numberForName["chulsoo"] = 500
////print(numberForName)
//
////var names: Set<String> = []
//
////var names: Set<String> = ["yagom", "chulsoo", "younghee", "yagom"]
////var numbers = [100, 200, 300]
////
////print(type(of: names))
////print(type(of: numbers))
////
////print(names.isEmpty)
////print(names.count)
//
////let englishStudents: Set<String> = ["john", "chulsoo", "yagom"]
////let koreanStudents: Set<String> = ["jenny", "yagom", "chulsoo", "hana", "minsoo"]
////
////let intersectSet: Set<String> = englishStudents.intersection(koreanStudents)
////print(intersectSet)
////
////let symmetricDiffSet = englishStudents.symmetricDifference(koreanStudents)
////print(symmetricDiffSet)
////
////let unionSet = englishStudents.union(koreanStudents)
////print(unionSet)
////
////let subtractSet = englishStudents.subtracting(koreanStudents)
////print(subtractSet)
//
////enum ArithmeticExpression {
////    case number(Int)
////    indirect case addition(ArithmeticExpression, ArithmeticExpression)
////}
////
////let five = ArithmeticExpression.number(5)
////let four = ArithmeticExpression.number(4)
////let sum = ArithmeticExpression.addition(five, four)
////
////func evaluate(_ expression: ArithmeticExpression) -> Int {
////    switch expression {
////
////    case .number(let value):
////        return value
////
////    case .addition(let left, let right):
////        return evaluate(left) + evaluate(right)
////    }
////}
////
////let result: Int = evaluate(sum)
////print(result)
////
////enum Condition: Comparable {
////    case terrible
////    case bad
////    case good
////    case great
////}
////
////let myCondition: Condition = Condition.great
////let yourCondition: Condition = Condition.bad
////
////if myCondition >= yourCondition{
////    print("my condition is better")
////} else {
////    print("your condition is better")
////}
////
////
////
////
////
////enum Device: Comparable {
////    case iPhone(version: String)
////    case iPad(version: String)
////    case macBook
////    case iMac
////}
////
////var devices: [Device] = []
////devices.append(Device.iMac)
////devices.append(Device.iPhone(version: "14.3"))
////devices.append(Device.iPad(version: "6.1"))
////devices.append(Device.macBook)
////
////let sortedDevices: [Device] = devices.sorted()
////print(sortedDevices)
//
////prefix operator**
////
////prefix func ** (value: Int) -> Int{
////    return value * value * value
////}
//
////var result = **5
////import Foundation
////
////infix operator **: MultiplicationPrecedence
////
////func ** (lhs: String, rhs: String) -> Bool{
////    return lhs.contains(rhs)
////}
////
////let hello: String = "hello yagom"
////let yagom: String = "yagom"
////let isContain:Bool = hello ** yagom
////
////print(isContain)
//
////var numbers: [Int] = [1,2,3]
////
////func nonRef(_ arr: [Int])
////{
////    var copiedArr: [Int] = arr
////    copiedArr[1] = 1
////}
////
////nonRef( numbers )
////print(numbers[0])
////print(numbers[1])
////print(numbers[2])
////
////func ref(_ arr: inout [Int])
////{
////    arr[1] = 1
////}
////
////ref( &numbers )
////print(numbers[0])
////print(numbers[1])
////print(numbers[2])
////
////typealias MoveFunc = (Int) -> Int
////
////func move(_ shouldGoLeft: Bool) -> MoveFunc {
////    func goRight(_ currentPos: Int) -> Int{
////        return currentPos + 1
////    }
////
////    func goLeft(_ currentPos: Int) -> Int{
////        return currentPos - 1
////    }
////
////    return shouldGoLeft ? goLeft : goRight
////}
////
////var position: Int = -4
////
////let moveToZero: MoveFunc = move(position > 0)
////print("go to origin")
////
////while position != 0{
////    print("\(position)")
////    position = moveToZero(position)
////}
//
////func crashAndBurn() -> Never{
////    fatalError("Something very, very bad happened")
////}
////
//////crashAndBurn()
////
////func somFunction(isAllIsWell: Bool){
////    guard isAllIsWell else{
////        print("there is a thief")
////        crashAndBurn()
////    }
////    print("Allis Well")
////}
////
////somFunction(isAllIsWell: true)
////somFunction(isAllIsWell: false)
//
////func say(_ something: String) -> String{
////    print(something)
////    return something
////}
////
////@discardableResult func discardableResultSay(_ something: String) -> String{
////    print(something)
////    return something
////}
////
////say("hello")
////discardableResultSay("hello")
//
////var name: String? = "yagom"
////print(name!)
////
////var na: Optional<String> = "kim"
////print(na)
////na = nil
////print(na)
////
////var num = nil
////
////num = nil
////
////
////func checkOptionalValue(value: Any?) {
////    switch value{
////    case .none:
////        print("nil")
////    case .some(let value):
////        print("value: \(value)")
////    }
////}
////
////var my: String? = "yagom"
////checkOptionalValue(value: my)
////
////my = nil
////checkOptionalValue(value: my)
////
//
////struct Point{
////    var x: Int = 0
////    var y: Int = 0
////}
////
////class Position{
////    lazy var point: Point = Point()
////    let name: String
////
////    init(name: String) {
////        self.name = name
////    }
////}
////
////let xyPosition: Position = Position(name: "yagom")
////
////print(xyPosition.point)
////
////struct Point{
////    var x: Int
////    var y: Int
////
////    var oppositePoint: Point{
////        get{
////            return Point(x: -x, y: -y)
////        }
//////        set(opposite){
//////            x = -opposite.x
//////            y = -opposite.y
//////        }
////        set{
////            x = -newValue.x
////            y = -newValue.y
////        }
////    }
////}
////
////var position: Point = Point(x: 10, y: 20)
////
////print(position)
////
////print(position.oppositePoint)
////
////position.oppositePoint = Point(x: 15, y: 10)
////
////print(position)
////
////
////class LevelClass{
////    var level: Int = 0{
////        didSet{
////            print("\(level)")
////        }
////    }
////
////    func levelUp(){
////        print("level up")
////        level += 1
////    }
////
////    func levelDown(){
////        print("level down")
////        level -= 1
////        if level < 0{
////            reset()
////        }
////    }
////
////    func jumpLevel(to: Int){
////        print("jump to \(to)")
////        level = to
////    }
////
////    func reset(){
////        print("reset")
////        level = 0
////    }
////}
////
////
////var levelInstance: LevelClass = LevelClass()
////levelInstance.levelUp()
////levelInstance.levelDown()
////levelInstance.levelDown()
////levelInstance.jumpLevel(to: 3)
////
////struct Puppy{
////    var name: String = "Dog"
////
////    func callAsFunction(){
////        print("bark bark")
////    }
////
////    func callAsFunction(destination: String){
////        print("\(destination)")
////    }
////
////    func callAsFunction(something: String, times: Int){
////        print("\(something), \(times)")
////    }
////
////    func callAsFunction(color: String) -> String{
////        return "\(color) poop"
////    }
////
////    mutating func callAsFunction(name: String){
////        self.name = name
////    }
////}
////
////var doggy: Puppy = Puppy()
////doggy.callAsFunction()
////doggy()
////doggy.callAsFunction(destination: "house")
////doggy(destination: "home")
////doggy(something: "kill", times: 1)
////
////class AClass {
////    static func typeMethod(){
////        print("Aclass typemethod")
////    }
////
////    class func classtypeMethod(){
////        print("Aclass classtypeMethod")
////    }
////}
////
////class BClass: AClass {
////    override static func classtypeMethod() {
////        print("child class")
////    }
////}
////
////AClass.typeMethod()
////AClass.classtypeMethod()
////BClass.typeMethod()
////BClass.classtypeMethod()
////
////struct SystemVolume{
////    static var volume: Int = 5
////
////    static func mute(){
////        self.volume = 0
////    }
////}
////
////class Navigation{
////    var volume: Int = 5
////
////    func guideWay(){
////        SystemVolume.mute()
////    }
////
////    func finishGuideWay(){
////        SystemVolume.volume = self.volume
////    }
////}
////
////SystemVolume.volume = 10
////let myNavi: Navigation = Navigation()
////
////myNavi.guideWay()
////print(SystemVolume.volume)
////
////myNavi.finishGuideWay()
////print(SystemVolume.volume)
//
////let optionalArray: [Int]? = [1,2,3]
////optionalArray?[1]
////
////var optionalDictionary: [String: [Int]]? = [String: [Int]]()
////optionalDictionary?["numberArray"] = optionalArray
////optionalDictionary?["numberArray"]?[2]
////
////
////for i in 0...4{
////    guard i == 2 else{
////        continue
////    }
////    print(i)
////}
////
////let numbers: [Int] = [0,0,0,0,0,0]
////
////var doubled: [Int] = [Int]()
////var strings: [String] = [String]()
//
////doubled = numbers.map({(number: Int) -> Int in return number * 2})
////print(doubled)
////
////strings = numbers.map({ (number: Int) -> String in return "\(number)"})
////print(strings)
//
////doubled = numbers.map({return $0 * 2})
////print(doubled)
////
////doubled = numbers.map({$0 * 2})
////print(doubled)
////
////let randomNumber: (Int) -> Int = { _ in Int.random(in: 1...45)}
////let rand = numbers.map(randomNumber)
////print(rand)
////
////let alphabetDictionary: [String: String] = ["a": "A", "b": "B"]
////
////var keys: [String] = alphabetDictionary.map{(tuple:(String, String)) ->
////    String in
////    return tuple.0
////}
////
////keys = alphabetDictionary.map{$0.0}
////let values: [String] = alphabetDictionary.map{ $0.1 }
////
////print(keys)
////print(values)
//
////let numbers: [Int] = [0,1,2,3,4,5]
////
////let even: [Int] = numbers.filter{(number: Int) -> Bool in return number % 2 == 0}
////print(even)
////
////let odd: [Int] = numbers.filter{ $0 % 2 == 1}
////print(odd)
////
////let oddnum: [Int] = numbers.map{$0 + 3}.filter{$0 % 2 == 1}
////print(oddnum)
//
////let numbers: [Int] = [1,2,3]
////
////var sum: Int = numbers.reduce(0, {(result: Int, next: Int) -> Int in
////    print("\(result) + \(next)")
////
////    return result + next
////})
////
////print(sum)
////
////let sumforThree: Int = numbers.reduce(3){ return $0 + $1}
////print(sumforThree)
//
////import Foundation
////import UIKit
////
////let numbers: [Int] = [1,2,3,4,5,6,7]
////
////var result: Int = numbers.filter{$0.isMultiple}
////
////enum Gender{
////    case male, female, unknown
////}
////
////struct Friend{
////    let name: String
////    let gender: Gender
////    let location: String
////    var age: UInt
////}
////
////var friends: [Friend] = [Friend]()
////
////friends.append(Friend(name: "Yoobato", gender: .male, location:"bally", age: 25))
////friends.append(Friend(name: "Jisoo", gender: .male, location:"sydney", age: 24))
////friends.append(Friend(name: "Juhyun", gender: .male, location:"gyeonggy", age: 30))
////friends.append(Friend(name: "Jiyoung", gender: .female, location:"Seoul", age: 22))
////friends.append(Friend(name: "SungHo", gender: .unknown, location:"ChoongBook", age: 20))
////friends.append(Friend(name: "JungKi", gender: .male, location:"Daejun", age: 29))
////
////
////var result: [Friend] = friends.map{Friend(name: $0.name, gender: $0.gender, location: $0.location, age: $0.age + 1)}
////result = result.filter{$0.location != "서울" && $0.age >= 25}
////
////let string: String = result.reduce("Seoul 25") {
////    $0 + "\n" + "\($1.name) \($1.gender) \($1.location) \($1.age)세"
////}
////
////print(string)
////
////var value: Int? = 2
////value.map{$0 + 3}
////value
////value = nil
////value.map{$0 + 3}
////
//
////var colorArray = ["red", "green", "black", "blue", "yellow", "red", "green", "yellow", "red", "green", "grey", "purple", "orange", "blue", "yellow", "orange", "black", "yellow", "red"]
////
////func getMostCommonColor1(array: [String]) -> [String]{
////
////    var topColors: [String] = []
////    var colorDictionary: [String: Int] = [:]
////
////    for color in array{
////        if let count = colorDictionary[color]{ // nil이 아니라면,
////            colorDictionary[color] = count + 1 //기존 카운트에 1을 더하라
////        }else { // nil이라면,
////            colorDictionary[color] = 1 // 1을 추가해준다.
////        }
////    }
////
////    let highestValue = colorDictionary.values.max() // value중 가장 높은 값은?
////
////    for (color, _) in colorDictionary{ // colorDictionary안에 있는 만큼
////        if colorDictionary[color] == highestValue{ // 해당 색과 highestValue가 같다면,
////            topColors.append(color) //topColor array에 대입
////        }
////    }
////
////    return topColors
////}
////
////print(getMostCommonColor1(array: colorArray))
////
////
////
////func getMostCommonColors(array: [String]) -> [String]{
////
////    var topColors: [String] = []
////    var colorDictionary: [String: Int] = [:]
////
////    for color in array{
////        if let count = colorDictionary[color]{
////            colorDictionary[color] = count + 1
////        } else {
////            colorDictionary[color] = 1
////        }
////    }
////
////    let highestValues = colorDictionary.values.max()
////
////    for (color, count) in colorDictionary{
////        if colorDictionary[color] == highestValues{
////            topColors.append(color)
////        }
////    }
////
////    return topColors
////}
////
////print(getMostCommonColors(array: colorArray))
////
////var prizeList: [Int : Int] = [5000 : 0, 50000 : 0, 1500000 : 0, 30000000 : 0, 200000000 : 0]
////
////for (prize, concurrentCount) in prizeList{
////
////}
////
////print(prizeList)
////
////import Foundation
////import UIKit
////
////// split array
////func mergeSort(array: [Int]) -> [Int]{
////
////    guard 1 < array.count else{
////        return array    //count가 1보다 적다면 여기로 들어 간다.
////    }
////
////    let leftArrays = Array(array[0..<array.count/2])
////    let rightArrays = Array(array[array.count/2..<array.count])
////
////    return merge(left: mergeSort(array: leftArrays), right: mergeSort(array: rightArrays))
////}
////
////// merge array
////func merge(left: [Int], right: [Int]) -> [Int]{
////
////    var mergeArray: [Int] = []
////    var left = left
////    var right = right
////
////    while 0 < left.count && 0 < right.count{
////
////        if left.first! < right.first! {
////            mergeArray.append(left.removeFirst())
////        } else {
////            mergeArray.append(right.removeFirst())
////        }
////    }
////
////    return mergeArray + left + right
////}
////
////var numbers: [Int] = []
////
////for _ in 0..<50{
////    let randomInt = Int.random(in: 1..<1000)
////    numbers.append(randomInt)
////}
////print("Not sorted: \(numbers)")
////print("sorted : \(mergeSort(array: numbers))")
////
////numbers = [9,3,10,11,7,4]
////
////var numbers1 = numbers.sorted()
////
////print(numbers)
////print(numbers1)
////
////
////extension Array{
////    subscript(safe index:Int) -> Element?{
////        return indices ~= index ? self[index] : nil
////    }
////}
////
////struct concurr{
////    var numbers: [Int] = [0,0,0,0,0]
////
////    mutating func increase3(){
////        numbers[0] += 1
////    }
////}
////
////var concurrent = concurr()
////
////concurrent.numbers[0] = 1
//////concurrent.numbers[0] += 1
////concurrent.increase3()
////
////print(concurrent.numbers)
//
////let array = [1,2,3,4]
////var index = 0
////var arr = array.forEach{
////    $0
////    index = index + 1
////}
////print(arr)
////print(index)
//
//
////
//// foreach, map, compact map, flat map, reduce
//
////var numbers: [Int] = [0,1,2,3,4]
//
////var doubleNumbers: [Int] = [Int]()
////var strings: [String] = [String]()
////
////doubleNumbers = numbers.map({//(number: Int) -> Int in
////        return $0 * 2
////})
////
////let combination = numbers.reduce(0, *)
////print(combination)
//
//
////numbers = numbers.filter({return $0 % 2 == 0})
////
////print(numbers)
//
////numbers = numbers.map({$0 * 2})
////print(numbers)
////
////let number = numbers.reduce(0, {runningSum, value in
////    runningSum + value
////})
////
////print(number)
//
////class Person{
////    let name: String
////    let isAdult: Bool
////
////    init(name: String, isAdult: Bool){
////        self.name = name
////        self.isAdult = isAdult
////    }
////}
////
////var array = [
////    Person(name: "John", isAdult: true),
////    Person(name: "kim", isAdult: false),
////    Person(name: "park", isAdult: true),
////    Person(name: "lee", isAdult: true),
////    Person(name: "Noh", isAdult: false),
////    Person(name: "Ha", isAdult: false),
////    Person(name: "Jo", isAdult: true),
////    Person(name: "Choi", isAdult: true)
////]
////
////array = array.filter({return $0.isAdult })
////let namesOfAdults = array.map({$0.name})
////
////print(namesOfAdults)
//
//// reduce  = 하나로 합쳐준다.
//// map  = for 처럼 모든 []요소에 접근해서 뭐를 해주고 []로 넣어준다.
//// filter  = true false 조건으로 []에 있는 것을 걸러서 다른 []에 담아 준다.
////
////var numbers = [0,1,2,3,4,5,6]
////
////var doubleNumbers = numbers.map{ $0 * 2 }
////
////let pairs = [[1,5], [7,9], [11,15]]
////var sums = pairs.map{ $0[0] + $0[0]}
////print(sums)
////
////let groups = [[1,3,9,11], [5,7]]
////var groupSum = groups.map{(group) -> Int in
////    var sum = 0
////    for num in group {
////        sum += num
////    }
////    return sum
////}
////
////groupSum
////
////let stringValues = ["1", "two", "3", "4", "five"]
////let intValues = stringValues.map{ Int($0) }
////intValues
//
////
////struct User{
////    var name: String
////    var age: Int
////}
////
////let users = [
////    User(name: "Curly", age: 15),
////    User(name: "Larry", age: 18),
////    User(name: "Moe", age: 18)
////]
////
////let names = users.map{ $0.name }
////names
////
////let tempAvgC: [String: Double] = ["Spring": 13, "Summer": 22, "Fall": 14, "Winter": 7]
////
//////let tempAvgF = tempAvgC.map{(season, temp) in [season: temp * 9 / 5 + 32]}
////
////let tempAvgF = tempAvgC.map{[$0 : $1 * 9 / 5 + 32]}
////print(tempAvgF)
//
////let dataDict: [String: Int] = ["Curly": 18, "Larry": 22, "Moe": 7]
////struct User{
////    var name: String
////    var age: Int
////}
////
////var users = dataDict.map{User(name: $0, age: $1)}
////dump(users)
////
////let fahrTemps: Set = [-40, 0, 32, 90, 212]
////
////let celTemp = Set(fahrTemps.map{($0 - 32) * 5/9})
////print(celTemp)
//
////let numbers1 = [1,2,3,4,5,6]
////var sum = numbers1.reduce(0) {(current, number)-> Int in
////    current + number }
////var sum1 = numbers1.reduce(0) {$0 + $1}
////print(sum1)
////
////var sum2 = numbers1.reduce(0, +)
////
////let numberGroups = [[2,6,8,10], [18, 5, 11], [5,15,8,11,18]]
////let groupSum = numberGroups.flatMap{$0}.reduce(0, +)
////print(groupSum)
////
////
////struct Employee {
////    var name: String
////    var salary: Double
////}
////
////let employees = [
////    Employee(name: "Mary", salary: 100_110),
////    Employee(name: "jack", salary: 50_000),
////    Employee(name: "Sally", salary: 75_500),
////    Employee(name: "Fred", salary: 48_900)
////]
////
////let totalSalary = employees.reduce(0) {$0 + $1.salary}
////print(totalSalary)
////
////let employeeDict = ["Mary": 100_000, "Jack": 50_000, "Sally": 75_500, "Fred": 35_500]
////let salaryTotal = employeeDict.reduce(0) {$0 + $1.value}
////print(salaryTotal)
////
////let names = employeeDict.reduce("") {$0 + $1.key + " "}
////print(names)
////
////
////let weights: Set = [98.5, 102.7, 100.2, 88.4]
////let totalWeights = Set(arrayLiteral: weights.reduce(0) {$0 + $1})
////totalWeights
////let totalWeights1 = Set(arrayLiteral: weights.reduce(0, +))
////totalWeights1
////
////let heavyWeights = weights.reduce(0) {(currentTotal, weight) -> Double in
////    if 100 < weight {
////        return currentTotal + weight
////    } else{
////        return currentTotal
////    }
////}
////heavyWeights
////
////let heavyWeights1 = weights.reduce(0) { 100 < $1 ? ($0 + $1) : $0 }
////heavyWeights1
////
////
////let numbers = [1,2,3,4,5,6,7,8,9,10]
////let newNumbers = numbers.filter{(number)-> Bool in
////    number <= 5 }
////newNumbers
////let newNumbers1 = numbers.filter{$0 <= 5}
////newNumbers1
////
////let names = ["Alice", "Bert", "Allen", "Samantha", "Ted", "albert"]
////let aNames = names.filter{$0.uppercased().prefix(4) == "A"}
////aNames
////
////struct Person {
////    var name: String
////    var age: Int
////}
////let people = [
////    Person(name: "Curly", age: 16),
////    Person(name: "Larry", age: 22),
////    Person(name: "Moe", age: 12),
////    Person(name: "Shemp", age: 25)
////]
////
////let adults = people.filter{ 19 <= $0.age}
////print(adults)
////
////let specialPeople = people.filter { 20 < $0.age && $0.name.uppercased().prefix(1) == "M" }
////specialPeople
////
////let children = people.filter{ $0.age < 19 }.map{$0.name}
////children
////
////let peopleDict = ["Curly": 16, "Larry": 22, "Moe": 12, "Shemp": 25]
////let adultPeople = peopleDict.filter{$0.value >= 19}
////adultPeople
////
////let weights: Set = [98.5, 102.7, 100.2, 88.4]
////let notobese = Set(weights.filter{$0 < 100})
////notobese
//
////var numbers = [1,2,3,4,5,6,7,8,9,10]
////
//////
//////var peopleDict = ["Curly": 16, "Larry": 22, "Moe": 12, "Shemp": 25]
//////peopleDict.removeAllf
////
////var foundit = numbers.contains{$0.isMultiple(of: 7)}
////foundit
////
////struct Employee {
////    var name: String
////    var hourlyWage: Double
////}
////let employees = [
////    Employee(name: "Mary", hourlyWage: 22.5),
////    Employee(name: "Jack", hourlyWage: 17.5),
////    Employee(name: "Sally", hourlyWage: 22.90),
////    Employee(name: "Fred", hourlyWage: 12.50)
////]
////
////let needRaise = employees.contains { $0.hourlyWage < 15}
////needRaise
////
////let sentence = "the quick brown fox, jumped over the lazy Dog. And this is what he found"
////
////let contains = sentence.lowercased().contains("Dog")
////contains
//
////let numbers = [1,2,3,4,5,6,7,8,9,10]
////numbers.forEach {
////    $0.isMultiple(of: 2) ? print("\($0) is Even") : print("\($0) is Odd")}
////
////struct Person: Comparable {
////    static func < (lhs: Person, rhs: Person) -> Bool {
////        lhs.name < rhs.name
////    }
////
////    static func > (lhs: Person, rhs: Person) -> Bool {
////        lhs.age > rhs.age
////    }
////
////    var name: String
////    var age: Int
////}
////
////var people = [
////    Person(name: "Moe", age: 12),
////    Person(name: "Larry", age: 22),
////    Person(name: "Shemp", age: 25),
////    Person(name: "Curly", age: 16)
////]
////
//////let sortedName = people.sorted{$0.age > $1.age}
//////print(sortedName)
//////
////
////let sortedName = people.sorted()
////sortedName
////
////let sortedAge = people.sorted(by: >)
////sortedAge
////
////let peopleDict = ["Curly": 16, "Larry": 22, "Moe": 12, "Shemp": 25]
////
//////let sortedPeopleName = peopleDict.sorted{(p1, p2) -> Bool in p1.key < p2.key }
//////sortedPeopleName
////
////let sortedPeopleName = peopleDict.sorted{ $0.key < $1.key }
////sortedPeopleName
////let sortedPeopleAge = peopleDict.sorted{$0.value < $1.value}
////sortedPeopleAge
//
////func isValidSubsequence(_ array: [Int], _ sequence: [Int]) -> Bool {
////    // Write your code here.
////
////    let newArray = Set(array)
////    let newSequence = Set(sequence)
////
////    let intersectionNumber = newArray.intersection(newSequence)
////
////    if (intersectionNumber.count == newSequence.count){
////        return true
////    }
////
////    return false
////  }
////
////
////let array = [5, 1, 22, 25, 6, -1, 8, 10]
////
////let sequence = [5, 1, 22, 22, 25, 6, -1, 8, 10]
////
////print(isValidSubsequence(array, sequence))
//
////import UIKit
////
//////queue - main, global, custom
////
////// main
////DispatchQueue.main.async {
////    //UI update
////    let view = UIView()
////    view.backgroundColor
////
////}
////
////// global
////DispatchQueue.global(qos: .userInteractive).async {
////    //지금 당장 행야하는 것
////}
////
////DispatchQueue.global(qos: .userInitiated).async {
////    //거의 바로 해야하는 것, 사용자가 결과를 기다린다
////}
////
////DispatchQueue.global(qos: .utility).async {
////    //시간이 좀 걸리는 일들, 사용자가 당장 기다리지 않는 것, 네트워킹, 큰파일 불러올때
////}
////
////DispatchQueue.global(qos: .background).async {
////    // 사용자한테 당장 인식될 필요가 없는 것들, 뉴스 데이터 미리 받기, 위치 업데이트, 영상 다운 받는 다던지
////}
////
////
//////custom Queue
////let concurrentQueue = DispatchQueue(label: "concurrent", qos: .background, attributes: .concurrent)
////let serialQueue = DispatchQueue(label: "serial", qos: .background)
////
////
////
////
//////복합적인 상황
////func downloadImageFromServer() -> UIImage{
////    //heavy task
////
////    return UIImage()
////}
////
////func updateUI(image: UIImage){
////
////}
////
////DispatchQueue.global(qos: .background).async {
////    let image = downloadImageFromServer()
////
////    DispatchQueue.main.async {
////        updateUI(image: image)
////    }
////}
//
////
////import UIKit
////
////// URL
////let urlString = "http://connect-boxoffice.run.goorm.io/movies?order_type=1"
////let url = URL(string: urlString)
////
////url?.absoluteString
////url?.scheme //어떤 방식으로 네트워킹을 하고 있냐
////url?.host
////url?.path
////url?.query
////url?.baseURL
////
////var urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movies?")
////let order_typeQuery = URLQueryItem(name: "order_type", value: "1")
////
////urlComponents?.queryItems?.append(order_typeQuery)
////
////urlComponents?.url
////urlComponents?.string
////urlComponents?.queryItems
////
////
////struct UserData : Codable{
////
////    var grade : Int
////    var thum : String
////    var reservation_grade : Int
////    var title : String
////    var reservation_rate : Float
////    var user_rating : Float
////    var data : String
////    var id : String
////}
////
//
////let baseURL = URL(string:
////                    "https://connect-boxoffice.run.goorm.io")
////let relativeURL = URL(string: "/movies", relativeTo: baseURL)
////
////relativeURL?.absoluteString
////relativeURL?.scheme
////relativeURL?.host
////relativeURL?.path
////relativeURL?.query
////relativeURL?.baseURL
//
////let config = URLSessionConfiguration.default
////let session = URLSession(configuration: config)
////
//////var urlComponents = URLComponents(string: "connect-boxoffice.run.goorm.io/movies")
//////let query = URLQueryItem(name: "order_type", value: "1")
//////urlComponents?.queryItems?.append(query)
//////let requestURL = urlComponents?.url!
////
////let url = URL(string: "http://connect-boxoffice.run.goorm.io/movies?order_type=1")
////let request = URLRequest(url: url!)
////
////let dataTask = session.dataTask(with: request) {(data, response, error) in
////    guard error == nil else { return }
////
////    guard let statusCode = (response as? HTTPURLResponse)?.statusCode else { return }
////    let successRange = 200..<300
////
////    guard successRange.contains(statusCode) else{
////        return
////
////    }
////    guard let resultData = data else { return }
////    let resultString = String(data: resultData, encoding: .utf8)
////    print("--> \(resultData)")
////    print("\(String(describing: resultString))")
////}
//
////let config = URLSessionConfiguration.default
////let session = URLSession(configuration: config)
////
////var urlComponents = URLComponents(string: "https://itunes.apple.com/search?")!
////let mediaQuerry = URLQueryItem(name: "media", value: "music")
////let entityQuerry = URLQueryItem(name: "entity", value: "song")
////let termQuerry = URLQueryItem(name: "term", value: "지드래곤")
////urlComponents.queryItems?.append(mediaQuerry)
////urlComponents.queryItems?.append(entityQuerry)
////urlComponents.queryItems?.append(termQuerry)
//
//
////var urlComponents = URLComponents(string: "https://jsonplaceholder.typicode.com/posts")!
////let requestURL = urlComponents.url!
////
////let dataTask = session.dataTask(with: requestURL) { (data, response, error) in
////    guard error == nil else { return }
////
////    guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {return}
////    let successRange = 200..<300
////
////    guard successRange.contains(statusCode) else{
////        return
////    }
////
////    guard let resultData = data else { return }
////    let resultString = String(data: resultData, encoding: .utf8)
////
////    print("\(resultString)")
////}.resume()
//
////dataTask.resume()
//
//
//
//
////
////var urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movies?order_type=1")!
////let requestURL = urlComponents.url!
////
////let dataTask = session.dataTask(with: requestURL) { (data, response, error) in
////    guard error == nil else { return }
////
////    guard let statusCode = (response as? HTTPURLResponse)?.statusCode else {return}
////    let successRange = 200..<300
////
////    guard successRange.contains(statusCode) else{
////        return
////    }
////
////    guard let resultData = data else { return }
////    let resultString = String(data: resultData, encoding: .utf8)
////
////    print("\(resultString)")
////}
//
////import UIKit
////
////let config = URLSessionConfiguration.default
////let session = URLSession(configuration: config)
////
////struct Response : Codable{
////    let order_type : Int
////    let movies: [Movie]
////}
////
////struct Movie : Codable{
////        var reservation_rate : Double
////        var id : String
////        var thumb : String
////        var date : String
////        var title : String
////        var reservation_grade : Int
////        var user_rating : Double
////        var grade : Int
////}
////
//////
//////var urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movies")!
//////let requestURL = urlComponents.url!
//////
//////let dataTask = session.dataTask(with: requestURL) {(data, response, error) in
//////    guard error == nil else { return }
//////
//////    guard let statusCode = (response as? HTTPURLResponse)?.statusCode else { return }
//////    let successRange = 200..<300
//////
//////    guard successRange.contains(statusCode) else{
//////        return
//////    }
//////
//////    guard let resultData = data else {return }
////////    let resultString = String(data: resultData, encoding: .utf8)
//////
//////    do{
//////
//////        let decoder = JSONDecoder()
//////        let response = try decoder.decode(Response.self, from: resultData)
//////        let movies = response.movies
//////
////////        print("\(movies.count)")
////////
////////        //print("\(movies)")
////////        print("\(movies.first)")
////////
//////
//////        for index in 0..<movies.count {
//////            print("\(movies[index].reservation_grade)")
//////            print("\(movies[index].id)")
//////            print("\(movies[index].title)")
//////        }
//////
//////    } catch let error{
//////        print(error)
//////    }
//////
//////
//////    //print("\(resultString)")
//////}
//////
//////dataTask.resume()
////
//////struct Movie : Codable{
//////
//////    var grade : Int
//////    var thum : String
//////    var reservation_grade : Int
//////    var title : String
//////    var reservation_rate : Float
//////    var user_rating : Float
//////    var data : String
//////    var id : String
//////}
//////
//////struct Response: Codable{
//////    let order_type : Int
//////    let movies : [Movie]
//////}
////
////class SearchAPI {
////
////    var movies = [Movie]()
////
////    func load(){
////        let session = URLSession(configuration:  .default)
////
////        let urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movies")!
////
////        let requestURL = urlComponents.url!
////
////        let dataTask = session.dataTask(with: requestURL) { (data, response, error) in
////            let successRange = 200..<300
////
////            guard error == nil,
////                  let statusCode = (response as? HTTPURLResponse)?.statusCode,
////                  successRange.contains(statusCode) else {
////                    return
////            }
////
////            guard let resultData = data else{
////                return
////            }
////
////            self.movies = SearchAPI.parseMovies(resultData)
////
////            for index in 0..<self.movies.count{
////                print(self.movies[index].title)
////                print(self.movies[index].date)
////                print(self.movies[index].grade)
////                print(self.movies[index].id)
////                print(self.movies[index].reservation_grade)
////                print(self.movies[index].reservation_rate)
////                print(self.movies[index].thumb)
////            }
////        }
////        dataTask.resume()
////    }
////
////    static func parseMovies(_ data: Data) -> [Movie] {
////        let decoder = JSONDecoder()
////
////        do{
////            let response = try decoder.decode(Response.self, from: data)
////            let movies = response.movies
////            return movies
////        } catch let error{
////            print(error.localizedDescription)
////            return []
////        }
////    }
////}
////
////
////
////let sp = SearchAPI()
////sp.load()
//
//import UIKit
//
////DispatchQueue.global().sync{
////    sleep(2)
////    print("sync: Inside")
////}
////
////print("async: Outside")
//
////DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(5)){
////    print("first delay of 5sec")
////}
////
////DispatchQueue.main.asyncAfter(deadline: .now()+2){
////    print("delayed by 2second")
////}
//
////main thread vs background thread
//
////DispatchQueue.main.async{
////    foo()
////    print("main.async")
////} // done immidiately
////
////DispatchQueue.global(qos: .background).async {
////    //fetch data from api
////
////    //
////
////    DispatchQueue.main.async{
////        // update UI
////    }
////    foo()
////
////    print("global")
////}
////
////func foo(){
////
////}
//
////var completionHandlers: [() -> Void] = []
////
////func someFunctionWithEscapingClosure(completionHandler: @escaping () -> Void) {
////    completionHandlers.append(completionHandler)
////}
////
////class SomeClass {
////    var x = 10
////
////    func doSomething() {
////        someFunctionWithEscapingClosure { self.x = 100 }
////    }
////}
////
////let instance = SomeClass()
////completionHandlers.first?()
////print(instance.x) // Prints "100"
////
////
////func someFunctionWithNonescapingClosure(closure: @escaping () -> Void) {
////    // 2. do something...
////    // 3. 클로저가 실행된다.
////    closure()
////}
////
////class SomeClass {
////    var x = 10
////    func doSomething() {
////    // 1. 클로저를 함수의 인자로 전달한다.
////    someFunctionWithNonescapingClosure {
////        x = 200
////        // 4. 클로저가 종료되고, 클로저는 메모리에서 지워진다.
////    }
////}
////}
////
////let instance = SomeClass()
////instance.doSomething()
////print(instance.x) // Prints "200"
//
////let baseURL = URL(string: "http://connect-boxoffice.run.goorm.io/")
////let relativeURL = URL(string: "movie", relativeTo: baseURL)
////
////relativeURL?.absoluteString
////
////let id = "5a54c286e8a71d136fb5378e"
////
////var urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movie?")
////let idQuery = NSURLQueryItem(name: "id", value: id)
////
////urlComponents?.queryItems?.append(idQuery as URLQueryItem)
////urlComponents?.url
////
////class Contents: Decodable {
////    var contents: [Zedd]?
////}
////class Zedd: Decodable {
////    var name: String
////    var age: Int
////}
////
////
////let jsonString = """
////{
////"contents" : [
////    {
////        "name": null,
////         "age": 10
////
////    },
////    {
////        "name": "alan",
////        "age": 20
////    }
////]
////
////}
////""".data(using: .utf8)!
////
////
////let zedd = try! JSONDecoder().decode(Contents.self, from: jsonString)
////print(zedd.contents?[0].name, zedd.contents?[0].age) // nil Optional(10)
////print(zedd.contents?[1].name, zedd.contents?[1].age) // Optional("alan") Optional(20)
//
////struct Product: Encodable {
////    let name: String
////    let age: Int
////}
////
////let obj = Product(name: "Kim", age: 20)
////
////let encoder = JSONEncoder()
////encoder.outputFormatting = .prettyPrinted
////
////let data = try encoder.encode(obj)
////
////print(String(data: data, encoding: .utf8)!)
//
////
////struct AddNewInfo : Codable{
////
////    var ConfirmationCode: String?
////    var SsnOrTax: String?
////}
////
////struct Info : Codable {
////
////
////    var merchantType : Int?
////
////    var idx : Int?
////    var UserType : Int?
////    var Status : String?
////}
//
////
////let urlMerchantCheck = "connect-boxoffice.run.goorm.io/comment"
////let url        = URL(string: urlMerchantCheck)
////var request          = URLRequest(url: url!)
////request.httpMethod   = "POST"
////request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////print("request addValue done")
////
////let newInfo = AddNewInfo(ConfirmationCode : "", SsnOrTax : "")
////
////do {
////
////    let jsonBody = try JSONEncoder().encode(newInfo)
////    request.httpBody = jsonBody
////
////} catch {
////        print("error : ")
////        print(error)
////}
//
////let timestamp = NSDate().timeIntervalSince1970
////
////var st: String = "Bste!hetsi ogEAxpelrt x "
////var st2 = "AlgoExpert is the Best!"
////
////var newSt = st.sorted(by: <)
////var newst2 = st2.sorted(by: <)
////
////print(newSt)
////print(newst2)
////
////if (newSt == newst2){
////    print(true)
////}
////else {
////    print(false)
////}
////
//
//////MARK: Properties
////struct AddNewInfo : Codable {
////    var ConfirmationCode: String?
////    var SsnOrTax: String?
////}
////
////struct Info : Codable {
////    var merchantType : Int?
////
////    var idx : Int?
////    var UserType : Int?
////    var Status : String?
////}
////
////struct MerchantInfo : Codable {
////    var IsSuccess : Bool?
////    var Code : String?
////    var Message : String?
////    var ErrorMessage : String?
////    var ErrorStackTrace : String?
////    var list : String?
////
////    var key : AddNewInfo?
////    var obj : Info?
////}
////
////
////let urlMerchantCheck = "connect-boxoffice.run.goorm.io/comments?movie_id=5a54c286e8a71d136fb5378e"
////let url              = URL(string: urlMerchantCheck)
////var request          = URLRequest(url: url!)
////
////var merchantInfo: MerchantInfo
////
////request.httpMethod   = "POST"
////request.addValue("application/json", forHTTPHeaderField: "Content-Type")
////print("request done")
////
////// 1. To Wait for Task
////let semaphore = DispatchSemaphore(value: 0)
////
////do {
////    let jsonBody = try JSONEncoder().encode(merchantInfo.obj)
////    request.httpBody = jsonBody
////
////    print(jsonBody)
////} catch {
////    print(error)
////}
////
//
//
////let session = URLSession.shared
////let task = session.dataTask(with: request) { (data, _, _) in
////    guard let data = data else {return}
////
////
////    do {
////        //JSONDecoder
////        merchantInfo = try JSONDecoder().decode(MerchantInfo.self, from: data)
////        print(checkAPI)
////
////    } catch {
////        print(error)
////    }
////    //var backToString = String(data: data, encoding: String.Encoding.utf8) as String
////    //print(backToString)
////
////
////    // 2. Wait for Task
////    semaphore.signal()
////
////};  task.resume()
////
////// 3. Wait for Task
////semaphore.wait()
//
////public struct MovieComments: Codable{
////    let id: String
////    let rating: Double
////    let timestamp: Double
////    let writer: String
////    let movie_id: String
////    let contents: String
////}
////
////
////let myStruct = MovieComments(id: "", rating: 5.0, timestamp: 5.0, writer: "", movie_id: "", contents: "")
////let encoder = JSONEncoder()
////encoder.outputFormatting = .prettyPrinted
////
////let data = try! encoder.encode(myStruct)
////print(String(data: data, encoding: .utf8)!)
//
////struct Comment : Encodable{
////
////    let rating: Double
////    let writer: String
////    let movie_id: String
////    let contents: String
////}
////
////enum HttpMethod: String {
////        case post = "POST"
////        case get = "GET"
////        case put = "PUT"
////        case delete = "DELETE"
////}
////
////func requestTest(url: String, type: HttpMethod, body: Data? = nil) {
////    guard let url = URL(string: url) else { return }
////
////    // 1. 요청 객체 생성
////    var request = URLRequest(url: url)
////
////    // 2. 요청 방식
////    request.httpMethod = type.rawValue
////
////    // 3. 헤더
////    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
////
////    // 4. 바디
////    request.httpBody = body
////
////    // 5. Session
////    let session = URLSession.shared
////    let task = session.dataTask(with: request) { (data, response, error) in
////        guard error == nil else {
////            print(error!.localizedDescription)
////            return
////        }
////
////        if let response = response {
////           print(response)
////        }
////
////        do {
////
////            let anyData = try JSONSerialization.jsonObject(with: data!, options: [])
////            print(anyData)
////
////        } catch {
////            print(error.localizedDescription)
////        }
////
////    }
////    // 6. 실행
////    task.resume()
////}
////
////let info = Comment(rating: 5.0, writer: "KimKim", movie_id: "5a54c286e8a71d136fb5378e", contents: "HateLoveHateLove")
////
////do {
////
////    let jsonData = try JSONEncoder().encode(info)
////    requestTest(url: "http://connect-boxoffice.run.goorm.io/comment", type: .post, body: jsonData)
////
////} catch{
////    print(error)
////    print(error.localizedDescription)
////}
//
////public struct Movie : Codable {
////    let grade: Int
////    let thumb: String
////    let reservation_grade: Int
////    let title: String
////    let reservation_rate: Float
////    let user_rating: Float
////    let date: String
////    let id: String
////}
////
////public struct Response1: Codable{
////    let order: Int
////    let movies: [Movie]
////
////    enum CodingKeys: String, CodingKey{
////        case order = "order_type"
////        case movies
////    }
////
////    public init(from decoder: Decoder) throws{
////        let values = try decoder.container(keyedBy: CodingKeys.self)
////
////        order = try values.decode(Int.self, forKey: .order)
////        movies = try values.decode([Movie].self, forKey: .movies)
////    }
////}
////
////let urlComponents = URLComponents(string: "http://connect-boxoffice.run.goorm.io/movies?")
////
////
////func requestMoviesForTableViewGeneric(completion: @escaping (([Movie]) -> Void)){
////    if let requestURL = urlComponents?.url{
////        URLSession.shared.dataTask(with: requestURL){ (data, _, error) in
////            guard let receivedData = data else { return }
////            completion(parseMoviesGeneric(receivedData) as! [Movie])// as! [Movie])
////        }
////    }
////}
////
////
////func parseMoviesGeneric<T: Codable>(_ data: Data) -> T? {
////    let decoder = JSONDecoder()
////
////    do{
////        let response = try decoder.decode(T.self, from: data)
////        return response
////
////    } catch let error {
////        print(error.localizedDescription)
////        return nil
////    }
////}
//
//
////if let url = URL(string: "https://www.googleapis.com/youtube/v3/search?part=snippet&q=the weekend&key=AIzaSyBdpe9tBJwS1UPOBAOxsm7ycFGDfpcmtnY"){
////
////    URLSession.shared.dataTask(with: url){ (data, response, error) in
////        guard let data = data else { return }
////        print(data)
////    }
////} else{
////    print("error")
////}
//
////let url = URL(string: "https://www.googleapis.com/youtube/v3/search?part=snippet&q=the weekend&key=AIzaSyBdpe9tBJwS1UPOBAOxsm7ycFGDfpcmtnY")!
////
////let task = URLSession.shared.dataTask(with: url){(data, response, error) in
////    guard let data = data else { return }
////
////    do{
////        print(data)
////    }
////    catch {
////        let error = error
////        print(error.localizedDescription)
////    }
////}
//
//class Worker {
//    var iVar = 0
//    var operation: () -> Void
//
//    init(operation: @escaping () -> Void) {
//        self.operation = operation
//    }
//
//    func performOperation() {
//        self.operation()
//    }
//}
//
//class HTMLElement {
//
//    let name: String
//    let text: String?
//
////    lazy var asHTML: () -> String = { [weak self] in
////        if let text = self?.text {
////            return "<\(String(describing: self?.name))>\(text)</\(String(describing: self?.name))>"
////        } else {
////            return "<\(String(describing: self?.name)) />"
////        }
////    }
//
////    lazy var asHTML: () -> String = {
////        if let text = self.text {
////            return "<\(String(describing: self.name))>\(text)</\(String(describing: self.name))>"
////        } else {
////            return "<\(String(describing: self.name)) />"
////        }
////    }
//
////    func funcAsHTML() -> String {
////
////        if let text = self.text {
////
////            return "<\(String(describing: self.name))>\(text)</\(String(describing: self.name))>"
////
////        } else {
////
////            return "<\(String(describing: self.name)) />"
////
////        }
////    }
//
//    init(name: String, text: String? = nil) {
//        self.name = name
//        self.text = text
//    }
//
//    deinit {
//        print("\(name) is being deinitialized")
//    }
//
//}
//
//var paragraph: HTMLElement? = HTMLElement(name: "p", text: "hello, world")
//print(paragraph!.asHTML())
////print(paragraph?.funcAsHTML() ?? "")
////
//paragraph = nil

import UIKit


//let url = URL(string: "http://connect-boxoffice.run.goorm.io/movies?")!
//
//let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//    if let error = error {
//        print("error: \(error)")
//    } else {
//        if let response = response as? HTTPURLResponse {
//            print("statusCode: \(response.statusCode)")
//        }
//        if let data = data, let dataString = String(data: data, encoding: .utf8) {
//            print("data: \(dataString)")
//        }
//    }
//}
//task.resume()
//struct MovieCommentResponse: Decodable {
//    let movieId: String
//    let comments: [MovieComment]
//}
//
//struct MovieComment: Codable {
//    let id: String
//    let rating: Double
//    let timestamp: Double
//    let writer: String
//    let movie_id: String
//    let contents: String
//}

//let info = MovieComment(id: "", rating: 5.0, timestamp: 0.0, writer: "playground", movie_id: "5a54c286e8a71d136fb5378e", contents: "from playground")
//var data: Data?
//do{
//    let jsonData = try JSONEncoder().encode(info)
//    data = jsonData
//}
//
//let url = URL(string: "http://connect-boxoffice.run.goorm.io/comment")!
//
//var request = URLRequest(url: url)
//request.httpMethod = "POST"
//request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//request.httpBody = data
//
//URLSession.shared.dataTask(with: request) { (data, response, error) in
//    if let error = error {
//        print("error: \(error)")
//    } else {
//        if let response = response as? HTTPURLResponse {
//            print("statusCode: \(response.statusCode)")
//        }
//        if let data = data,
//           let dataString = String(data: data, encoding: .utf8) {
//            print("data: \(dataString)")
//        }
//    }
//}.resume()


//
//let url = URL(string: "http://connect-boxoffice.run.goorm.io/comment")
//var request = URLRequest(url: url!)
//request.httpMethod = "GET"
//request.setValue("application/json", forHTTPHeaderField: "Content-Type")
////request.httpBody = body
//
//URLSession.shared.dataTask(with: request) { (data, response, error) in
//    guard error == nil else {
//        print(error!.localizedDescription)
//        return
//    }
//
//    if let response = response {
//       print(response)
//    }
//
//    if let response = response as? HTTPURLResponse {
//        print("statusCode: \(response.statusCode)")
//    }
//
//}.resume()


//enum MoviesOrderType: CaseIterable {
//    case greatestReservationRate
//    case latestDate
//    case greatestUserRating
//
//    var title: String {
//        switch self {
//        case .greatestReservationRate:
//            return "예매율"
//        case .greatestUserRating:
//            return "큐레이션"
//        case .latestDate:
//            return "개봉일"
//        }
//    }
//}
//import Foundation
//import WebKit
//
//public struct APIConfiguration{
//    public let method: HTTPMethod
//    public let url: URL
//
//    public init(method: HTTPMethod = "GET",
//                baseURL: URL,
//                path: String,
//                parameters: [String: String]
//    ){
//        let url = baseURL.appendingPathComponent(path)
//        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
//
//        urlComponents?.queryItems = parameters.map{URLQueryItem(name: $0.key, value: $0.value)}
//
//        self.method = method
//        self.url = urlComponents?.url ?? url
//    }
//}
//
//var test = URLRequest(url: "http")
//test.
//

//
//let value1 = ["one", "two", "three", "four"]
//let value2 = ["kk", "hh", "ww"]
//
//let count1: Double = Double(value1.count)
//let count2: Double = Double(value2.count)
//
//print(value2.count / value1.count * 100)
//print(count2 / count1)

//let date = Date()
//let dateFormatter = DateFormatter()
//dateFormatter.dateFormat = "dd-MM-yyyy"
//let today = dateFormatter.string(from: date)
//print(today)
//
//
//let date = Date()
//let calendar = Calendar.current.timeZone
//
//let time = Calendar.current
//
////
////let year = calendar.component(.year, from: date)
////let month = calendar.component(.month, from: date)
////let day = calendar.component(.day, from: date)
////
//
//
//print(calendar)
//let date = Date()
//let calendar = Calendar.current
//let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
//print(components)
//print(date.timeIntervalSinceNow)

//
//let dateFormatter = DateFormatter()
//dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//
//let date = dateFormatter.date(from: dateFormatter.dateFormat)
//let calendar = Calendar.current
//// Here .minute and .second is added in below line which is missing in your code
//let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date!)
//
//let finalDate = calendar.date(from:components)
//let newDate = calendar.date(byAdding: .hour, value: 3, to: finalDate!)

//
//let date = Date()
//let calendar = Calendar.current
//
//let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
//print(components.description)
//
//let dateTimeString = components.description
//
//let inputDateFormatter = DateFormatter()
//inputDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
//let date1 = inputDateFormatter.date(from: dateTimeString) // "Jun 30, 2020 at 2:41 PM"
//print(date1)

//let exitDateTime = Date.distantPast
//let currentDateTime = Date()
//
//let formatter = DateFormatter()
//formatter.timeStyle = .long
//formatter.dateStyle = .long
//
//let currentDate = formatter.date(from: formatter.string(from: currentDateTime))
//let pastDate = formatter.date(from: formatter.string(from: exitDateTime))
//
//let interval = currentDateTime.timeIntervalSince(exitDateTime)
//
//let diffComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute, .second], from: pastDate!, to: currentDate!)
//let months = diffComponents.month
//let days = diffComponents.day
//let hours = diffComponents.hour
//let minutes = diffComponents.minute
//let seconds = diffComponents.second


//let HOME_TEAM_WON = 1
//
//func tournamentWinner(_ competitions: [[String]], _ results: [Int]) -> String {
//
//
//  return ""
//}
//
//func generateDocument(_ characters: String, _ document: String) -> Bool {
//  // Write your code here.
//
//    let c = characters.sorted()
//    let d = document.sorted()
//
//
//
//    if c == d {
//        return true
//    }
//
//  return false
//}
//
//let characters = "Bste!hetsi ogEAxpelrt x "
//let document = "AlgoExpert is the Best!"
//
//generateDocument(characters, document)
import Foundation

//extension String {
//    subscript(index: Int) -> String {
//        return "s"
//    }
//}

//func firstNonRepeatingCharacter(_ string: String) -> Int {
//
//    if string.isEmpty{
//        return -1
//    }
//    var count = 0
//    var counts: [Int] = []
//
//    for index1 in 0..<string.count{
//        let strIndex1 = string.index(string.startIndex, offsetBy: index1)
//
//        for index2 in 0..<string.count {
//            let strIndex2 = string.index(string.startIndex, offsetBy: index2)
//
//            if string[strIndex1] == string[strIndex2] {
//                count += 1
//            }
//        }
//        counts.append(count)
//        count = 0
//    }
//    print(counts)
//    return counts.firstIndex(of: 1) ?? -1
//}
//
//let string = "abcdcaf"
//let string2 = "ababacc"
//let string3 = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
//firstNonRepeatingCharacter(string3)

//func classPhotos(_ redShirtHeights: inout [Int], _ blueShirtHeights: inout [Int]) -> Bool {
//
//    redShirtHeights.sort()
//    blueShirtHeights.sort()
//
//    var firstRowShirtColor = "blue"
//    if redShirtHeights[0] < blueShirtHeights[0] {
//        firstRowShirtColor = "red"
//    }
//
//    for index in 0..<redShirtHeights.count {
//        if firstRowShirtColor == "blue"{
//            if blueShirtHeights[index] < redShirtHeights[index] {
//
//            }
//            else {
//                return false
//            }
//        }
//        else if firstRowShirtColor == "red"{
//            if blueShirtHeights[index] > redShirtHeights[index] {
//
//            }
//            else {
//                return false
//            }
//        }
//    }
//
//    return true
//}
//
//var redShirtHeights = [2, 4, 7, 5, 1]
//var blueShirtHeights = [3, 5, 6, 8, 2]
//
//classPhotos(&redShirtHeights, &blueShirtHeights)

//func isMonotonic(array: [Int]) -> Bool {
//    // Write your code here.
//
//    print(array.size() - 1)
//
//    return false
//  }

//func findThreeLargestNumbers(array: [Int]) -> [Int] {
//    // Write your code here.
//    var container = array
//    var maxElements: [Int] = []
//    var maxElement = container[0]
//
//    while true {
//        maxElement = container[0]
//
//        for index in 1..<container.count {
//            print("index in loop: \(index)")
//            if maxElement < container[index]{
//                maxElement = container[index]
//            }
//        }
//
//        let index = container.firstIndex(of: maxElement) ?? 0
//        container.remove(at: index)
//        maxElements.append(maxElement)
//
//        if maxElements.count == 3 {
//            maxElements.sort()
//            return maxElements
//        }
//    }
//}
//
//findThreeLargestNumbers(array: [141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7])

//
//func groupAnagrams(_ words: [String]) -> [[String]] {
//    var sortedWords: [String] = []
//    var indexCollector: [[Int]] = []
//
//    for index in 0..<words.count{
//        sortedWords.append(String(words[index].sorted()))
//    }
//    print(sortedWords)
//
//    for index1 in 0..<words.count-1 {
//        var temp: [Int] = []
//
//        for index2 in 1..<words.count {
//            if sortedWords[index1] == sortedWords[index2]{
//                temp.append(index1)
//                temp.append(index2)
//            }
//        }
//        temp = Array(Set(temp))
//        indexCollector.append(temp)
//        temp.removeAll()
//    }
//
//    indexCollector = Array(Set(indexCollector))
//    print(indexCollector)
//
//    return []
//}
//
//var anagrams = [String: [String]]()
//var ana: [String: [String]] = [:]
//
//anagrams["ha"]?.append("kill")
//ana["d"]?.append("me")
//
//var w = Array<String>()
//w.append("kim")
//w.append("kim")
//
//w.description
//w.popLast()
//
//let words = ["yo", "act", "flop", "tac", "foo", "cat", "oy", "olfp"]
//
//groupAnagrams(words)





//
//func subarraySort(array: [Int]) -> [Int] {
//    let filteredIndices = zip(array, array.sorted())
//                            .enumerated()
//                            .filter {
//                                print($0.offset)
//                                print($0.element)
//                                return $0.element.0 != $0.element.1
//
//                            }
//        .map { print($0.element) }
//
//    zip(array, array.sorted()).enumerated().filter { element in
//        print(element.element.0)
//        print(element.element.1)
//        return true
//    }
//
//    //return [filteredIndices.first ?? -1, filteredIndices.last ?? -1]
//    return []
//}
//
//
//let array = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
//
//subarraySort(array: array)

//
//func quickSort(_ array: [Int]) -> [Int] {
//
//
//
//    return qs(array)
//  }
//
//
//func qs(_ array: [Int]) -> [Int] {
//
//    guard array.count > 1 else {return array}
//
//    let pivot = array[array.count / 2]
//    let left = array.filter { $0 < pivot }
//    let equal = array.filter{ $0 == pivot}
//    let right = array.filter { pivot < $0 }
//
//    return qs(left) + equal + qs(right)
//}aaA
//
////
//let array = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
//
//quickSort(array)

//
//let date = Date()
//let formatter = DateFormatter()
//formatter.dateFormat = "yyyy-MM-dd"
//
//let result = formatter.string(from: date)
//

//
//let todayDate = Date()
//let formatter = DateFormatter()
//formatter.dateFormat = "yyyy-MM-dd"
//formatter.string(from: todayDate)
//
//formatter.date(from: "2013-")
////
//
//
//let todayDate = Date()
//let formatter = DateFormatter()
//formatter.dateFormat = "yyyy-MM-dd"
//
//todayDate.description


//let nowUTC = Date()
//let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
//Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC)
//
//Date().description(with: .current)
//
//var dic = ["Kill": 8, "Da": 10]
//
//print(dic["Kill"])
//print(dic["Da"])
//
////dic.remove(at: 0)
//dic.removeValue(forKey: "Kill")
//
//print(dic["Kill"])
//print(dic["Da"])
//
//
//dic.removeAll()
//
//dic.updateValue(8, forKey: "kim")
//
//print(dic)
//
//struct WorkOutWithDate: Codable {
//    //var date: String
//    //var workOutWithTime: [WorkOutWithTime]
//
//    //                                  date     workouts
//    var selectedDateWorkOutWithTime: [String : [WorkOutWithTime]]?
//}
//
//struct WorkOutWithTime: Codable {
//    var time: String
//    var workOut: [WorkOut]
//}
//
//struct WorkOut: Codable, Equatable {
//    var workOutName: String
//    var reps: Int
//    var weights: Double
//    var isDone: Bool
//
//    init(time: String, contents: String, reps: Int, weights: Double, isCompleted: Bool) {
//        self.workOutName = contents
//        self.reps = reps
//        self.weights = weights
//        self.isDone = isCompleted
//    }
//}
//
//class CalendarManager {
//    static var shared = CalendarManager()
//
//    //var workOutInADayArray: [WorkOutInADay] = []
//
//    //var workOutInADayArray: [String : [WorkOutWithTime]] = [:]
//
//    var workOutWithDate = WorkOutWithDate()
//
//    private init(){}
//}
//
//CalendarManager.shared.workOutWithDate.selectedDateWorkOutWithTime?.updateValue([WorkOutWithTime(time: Date().description(with: .current), workOut: [WorkOut(time: "01", contents: "Bench", reps: 12, weights: 2, isCompleted: true)])], forKey: Date().description(with: .current))
//
//print(CalendarManager.shared.workOutWithDate)
//print(CalendarManager.shared.workOutWithDate.selectedDateWorkOutWithTime)


struct LikedImageInformation: Codable {
    var id: [String: String]
    
    init(){
        id = [String: String]()
    }
}

class LikedImageManager{
    static var shared = LikedImageManager()
    
    var likedImagesInformation = LikedImageInformation()
    
    
}

LikedImageManager.shared.likedImagesInformation.id["KZe02gpoAj4yVjxKQt"] = "KZe02gpoAj4yVjxKQt"

LikedImageManager.shared.likedImagesInformation.id.updateValue("KZe02gpoAj4yVjxKQt", forKey: "KZe02gpoAj4yVjxKQt1")


